//
//  StartViewModelTest.swift
//  HustleTests
//
//  Created by  Wasi Tariq on 06/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import XCTest
@testable import Hustle

class StartViewModelTest: XCTestCase {

    private let viewModel = StartViewModel()
    
    func testPhoneNumberWithCountryCodeAndArabSelected(){
        viewModel.setCountryCode(at: 0)
        let number = viewModel.phoneNumber(with: "+966324330544")
        assert(number == "966324330544", "Test Failed")
    }
    
    func testPhoneNumberWithZeroAndArabSelected(){
        viewModel.setCountryCode(at: 0)
        let number = viewModel.phoneNumber(with: "0324330544")
        assert(number == "966324330544", "Test Failed")
    }
    
    func testPhoneNumberWithoutZeroAndArabSelected(){
        viewModel.setCountryCode(at: 0)
        let number = viewModel.phoneNumber(with: "324330544")
        assert(number == "966324330544", "Test Failed")
    }
    
    func testPhoneNumberWithCountryCodeAndPakSelected(){
        viewModel.setCountryCode(at: 1)
        let number = viewModel.phoneNumber(with: "923243305445")
        assert(number == "923243305445", "Test Failed")
    }
    
    func testPhoneNumberWithZeroAndPakSelected(){
        viewModel.setCountryCode(at: 1)
        let number = viewModel.phoneNumber(with: "03243305445")
        assert(number == "923243305445", "Test Failed")
    }
    
    func testPhoneNumberWithoutZeroAndPakSelected(){
        viewModel.setCountryCode(at: 1)
        let number = viewModel.phoneNumber(with: "3243305445")
        assert(number == "923243305445", "Test Failed")
    }
    
}
