//
//  RideRequestDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 17/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct RideRequestDataModel: Decodable{
    var status: String?
    var ride: RideDataModel?
    var rideStatus: RideStatusDataModel?
}

struct RideDataModel: Decodable{
    var Id: String?
}

struct RideStatusDataModel: Decodable{
    var Id: String?
    var Number: Double?
    var Title: String?
}
