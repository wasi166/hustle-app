//
//  FavouritePlaceDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 21/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct FavouritePlaceDataModel: Decodable {
    var status: String?
    var lstFavouritePlaces: [placeModel]?
    var lstRecentLocations: [RecentLocationsModel]?
}

struct RecentLocationsModel: Decodable {
    var destionation: placeModel?
    var source: placeModel?
}

struct placeModel: Decodable {
    var Latitude: Double?
    var Longitude: Double?
    var Name: String?
}
