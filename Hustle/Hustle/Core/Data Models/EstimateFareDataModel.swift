//
//  EstimateFareDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 13/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct EstimateFareDataModel: Decodable {
    var status: String?
    var element: ElementDataModel?
    var lstRideType: [RideListDataModel]?
    
    func distance() -> String{
        return ("\((element?.Distance?.Value ?? 0.0)/1000) km")
    }
    
    func duration() -> String{
        return (element?.Duration?.Text) ?? ""
    }
    
    func carName(forIndexPath: IndexPath) -> String{
        return (lstRideType?[forIndexPath.section].lstCategory?[forIndexPath.row].Title) ?? ""
    }
    
    func estimateFare(forIndexPath: IndexPath) -> String{
        let minCost = (lstRideType?[forIndexPath.section].lstCategory?[forIndexPath.row].MinCost) ?? 0.0
        let maxCost = (lstRideType?[forIndexPath.section].lstCategory?[forIndexPath.row].MaxCost) ?? 0.0
        return "\(Int(minCost))-\(Int(maxCost))"
    }
    
    func getMaxCost(forIndexPath: IndexPath) -> String{
        let maxCost = (lstRideType?[forIndexPath.section].lstCategory?[forIndexPath.row].MaxCost) ?? 0.0
        return String(describing: maxCost)
    }
    
    func getMinCost(forIndexPath: IndexPath) -> String{
        let minCost = (lstRideType?[forIndexPath.section].lstCategory?[forIndexPath.row].MinCost) ?? 0.0
        return String(describing: minCost)
    }
    
    func rideID(forIndexPath: IndexPath) -> String{
        return (lstRideType?[forIndexPath.section].lstCategory?[forIndexPath.row].Id) ?? ""
    }
}

struct ElementDataModel: Decodable{
    var Duration: DistanceTimeDataModel?
    var Distance: DistanceTimeDataModel?
}

struct DistanceTimeDataModel: Decodable{
    var Value: Double?
    var Text: String?
}

struct RideListDataModel: Decodable{
    var Id: String?
    var Title: String?
    var lstCategory: [CategoryListDataModel]?
}

struct CategoryListDataModel: Decodable{
    var Id: String?
    var Title: String?
    var MinCost: Double?
    var MaxCost: Double?
    var Logo: String?
}
