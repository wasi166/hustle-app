//
//  RideDetailsDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 25/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct RideDetailsDataModel: Decodable{
    var Message: String?
}




/*
 Optional({
 DistADurBwCustADvr = "<null>";
 notificationType = 7;
 ride =     {
 DateTime = "/Date(1563637937429)/";
 Id = "363d0706-93db-40c4-8b9f-e3cebba5eecd";
 customer =         {
 EmailAddress = "<null>";
 FireBaseToken = "<null>";
 FirstName = "<null>";
 Id = "99b780c9-7fd3-4462-a45e-5486776de410";
 Image = "<null>";
 LastName = "<null>";
 PhoneNumber = "<null>";
 ratings = "<null>";
 status =             {
 Id = "F7AB1A26-6E88-49B3-B0AC-9F3F2AE2E7B3";
 Number = 0;
 Title = Busy;
 };
 };
 destination =         {
 Latitude = "24.8635707";
 Longitude = "67.0753158";
 Name = "Parsa Tower";
 };
 driver = "<null>";
 estimatedFare =         {
 Active = "<null>";
 DateTime = "7/20/2019 8:52:17 AM";
 EstimatedDistance = 4324;
 EstimatedMaxCost = "87.1605";
 EstimatedMinCost = "83.01000000000001";
 EstimatedTime = 54534;
 Id = "3407442e-c644-4cff-baf6-f0d4bc1afccc";
 };
 markArrived = "<null>";
 rideFare = "<null>";
 rideStatus =         {
 Id = "2052188C-2F8E-4E5B-A9C0-D9F49FF76AD7";
 Number = 1;
 Title = Requested;
 };
 rideTimings = "<null>";
 rideTypeCategory =         {
 BaseFare = "<null>";
 Id = "9E9AF54A-4DED-48CE-88E7-794E84F000F3";
 Logo = "<null>";
 PerKilometerCost = "<null>";
 PerTimeCost = "<null>";
 Tax = "<null>";
 Title = "Hustle Plus";
 TotalWaitingTime = "<null>";
 rideType = "<null>";
 };
 source =         {
 Latitude = "24.8940961";
 Longitude = "67.0631077";
 Name = "Askari Amusement Park";
 };
 };
 status = "Successfully sent the notifications";
 })
 
 */
