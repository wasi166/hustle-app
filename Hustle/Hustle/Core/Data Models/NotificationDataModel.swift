//
//  RideStatusDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 27/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation
import GooglePlaces

struct NotificationDataModel: Decodable{
    var status: String?
    var ride: RideModel?
    var DistADurBwCustADvr: DriverArrivalModel?
    var DistADurBwDvrADest: DriverArrivalModel?
}

struct RideModel: Decodable{
    var Id: String?
    var driver: RideDriverModel?
    var rideStatus: RideStatusModel?
    var source: LocationModel?
    var destination: LocationModel?
    var estimatedFare: EstimateFareModel?
    var rideFare: RideFareModel?
    var rideTypeCategory: RideCategoryModel?
}

struct RideDriverModel: Decodable{
    var Id: String?
    var FirstName: String?
    var LastName: String?
    var PhoneNumber: String?
    var CompletedRides: Int?
    
    var driverLocation: LocationModel?
    var driverVehicle: DriverVehicleModel?
    var ratings: DriverRatingModel?
    
    func getFullName() -> String{
        return "\(FirstName ?? "") \(LastName ?? "")"
    }
    
    func getRatingText() -> String{
        return "\(ratings?.AverageRating ?? 0.0)/5"
    }
    
    func getCompletedTripsText() -> String{
        return "Completed \(CompletedRides ?? 0) Rides."
    }
}

struct DriverVehicleModel: Decodable{
    var VehicleName: String?
    var VehicleColor: String?
    var ModelNumber: String?
}

struct DriverRatingModel: Decodable{
    var AverageRating: Double?
}

struct RideStatusModel: Decodable {
    var Number: Int?
    var Title: String?
    var Id: String?
}

struct LocationModel: Decodable{
    var Latitude: Double?
    var Longitude: Double?
    var Name: String?
    
    func getCoordinates() -> CLLocationCoordinate2D{
        return CLLocationCoordinate2D(latitude: Latitude ?? 0.0, longitude: Longitude ?? 0.0)
    }
}

struct DriverArrivalModel: Decodable{
    var status: String?
    var Duration: EstimateModel?
    var Distance: EstimateModel?
    
}

struct EstimateModel: Decodable {
    var Value: Double?
    var Text: String?
}

struct RideFareModel: Decodable{
    var TotalFare: Double?
}

struct EstimateFareModel: Decodable{
    var EstimatedMinCost: Double?
    var EstimatedMaxCost: Double?
    
    func getEstimatedCost() -> String{
        return "\(EstimatedMinCost ?? 0) - \(EstimatedMaxCost ?? 0) SAR"
    }
}

struct RideCategoryModel: Decodable{
    var Id: String?
    var Title: String?
}

