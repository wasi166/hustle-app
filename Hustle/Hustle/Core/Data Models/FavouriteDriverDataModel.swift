//
//  FavouriteDriverDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 29/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct FavouriteDriverDataModel: Decodable{
    var status: String?
    var lstDrivers: [DriverListDataModel]?
}

struct DriverListDataModel: Decodable{
    var Id: String?
    var FirstName: String?
    var LastName: String?
    var ratings: DriverRatingModel? // declared in notification data model file.
    var rideTypeCategory: RideCategoryModel? // declared in notification data model file.
    var CompletedRides: Int?
    
    func getFullName() -> String{
        return "\(FirstName ?? "") \(LastName ?? "")"
    }
    
    func getRatingText() -> String{
        return "\(ratings?.AverageRating ?? 0.0)/5"
    }
    
    func getCompletedTripsText() -> String{
        return "Completed \(CompletedRides ?? 0) Rides."
    }
}
