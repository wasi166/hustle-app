//
//  RideHistoryResponseDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 10/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct RideHistoryResponseDataModel: Decodable{
    var status: String?
    var customerDetails: CustomerDetailsDataModel?
}

struct CustomerDetailsDataModel: Decodable{
    var lstRideDetails: [RideHistoryDataModel]?
}

struct RideHistoryDataModel: Decodable{
    var Date: String?
    var Time: String?
    var destination: LocationModel?
    var source: LocationModel?
    var rideFare: RideFareModel?
    
}


