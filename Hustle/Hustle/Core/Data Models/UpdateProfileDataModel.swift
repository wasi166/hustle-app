//
//  UpdateProfileDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 10/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct UpdateProfileDataModel: Decodable{
    var status: String?
    var customer: CustomerData?
}
