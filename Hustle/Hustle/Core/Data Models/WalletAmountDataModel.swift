//
//  WalletAmountDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 07/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct WalletAmountDataModel: Decodable{
    var status: String?
    var customer: CustomerModel?
}

struct CustomerModel: Decodable{
    var customerWallet: CustomerWalletDataModel?
}

struct CustomerWalletDataModel: Decodable{
    var Amount: Double?
}
