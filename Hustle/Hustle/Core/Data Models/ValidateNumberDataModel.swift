//
//  ValidateNumberDataModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 03/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct ValidateNumberDataModel: Decodable{
    var status: String?
    var customerData: CustomerData?
    var token: String?
}

class CustomerData: Codable{
    var Id: String?
    var EmailAddress: String?
    var FirstName: String?
    var PhoneNumber: String?
    var LastName: String?
    var token: String?
}
