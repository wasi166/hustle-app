//
//  LocationSearchViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 20/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GooglePlaces

enum locationSearchMethod: Int{
    case search = 0
    case saved = 1
    case recent = 2
}

class SearchPlace{
    var name: String
    var longitude: Double
    var latitude: Double
    var formattedAddress = ""
    
    init(name: String, latitude: Double, longitude: Double, address: String = "") {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.formattedAddress = address
    }
}

protocol locationSearchDelegate {
    func didUpdateLocationData()
}

class LocationSearchViewModel: BaseViewModel {

    private let fetcher = GMSAutocompleteFetcher()
    private let fetcherToken = GMSAutocompleteSessionToken.init()
    private var searchedPlaces = [SearchPlace]()
    private var savedPlaces = [SearchPlace]()
    private var recentPlaces = [SearchPlace]()
    private var currentSearchMethod = locationSearchMethod.search
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchSavedPlaces()
        self.configureAutoCompleteFetcher()
    }
    
    private func configureAutoCompleteFetcher(){
        fetcher.delegate = self
        fetcher.provide(fetcherToken)
        
        let filter = GMSAutocompleteFilter()
        filter.country = countryCodeForSearch()
        fetcher.autocompleteFilter = filter
        fetcher.autocompleteBounds = autoCompleteBoundsForSearch()
    }
    
    private func autoCompleteBoundsForSearch() -> GMSCoordinateBounds{
        let lat = LocationManager.shared.lastReceivedLocation()?.latitude ?? 0.0
        let long = LocationManager.shared.lastReceivedLocation()?.longitude ?? 0.0
        let offset = 200.0 / 1000.0;
        let latMax = lat + offset;
        let latMin = lat - offset;
        let lngOffset = offset * cos(lat * Double.pi / 200.0);
        let lngMax = long + lngOffset;
        let lngMin = long - lngOffset;
        let initialLocation = CLLocationCoordinate2D(latitude: latMax, longitude: lngMax)
        let otherLocation = CLLocationCoordinate2D(latitude: latMin, longitude: lngMin)
        return GMSCoordinateBounds(coordinate: initialLocation, coordinate: otherLocation)
    }
    
    private func countryCodeForSearch() -> String{
        switch SessionManager.shared.getCurrentCountryCode() {
        case .arab:
            return "sa"
        case .pak:
            return "pk"
        }
    }
    
    func didChangeSearchText(text: String){
        fetcher.sourceTextHasChanged(text)
    }
    
    func didChangeSegmentValue(value: Int){
        if let method = locationSearchMethod(rawValue: value){
            self.currentSearchMethod = method
            (baseDelegate as? locationSearchDelegate)?.didUpdateLocationData()
        }
    }
    
    func numberOfSections() -> Int{
        return 1
    }
    
    func numberOfRows() -> Int{
        return [searchedPlaces, savedPlaces, recentPlaces][currentSearchMethod.rawValue].count
    }
    
    func dataForIndexPath(indexPath: IndexPath) -> SearchPlace{
        return [searchedPlaces, savedPlaces, recentPlaces][currentSearchMethod.rawValue][indexPath.row]
    }
    
    private func parametersForSavePlaces() -> Parameters{
        var params = Dictionary<String , Any>()
        params["customer"] = ["Id": SessionManager.shared.getID()]
        return Parameters(dictionary: params)
    }
    
    private func fetchSavedPlaces(){
        let service = Service(url: ServiceConstants.getSavedPlacesEndpoint, parameters: parametersForSavePlaces(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: FavouritePlaceDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                for place in model?.lstFavouritePlaces ?? []{
                    let savedPlace = SearchPlace(name: place.Name ?? "", latitude: place.Latitude ?? 0.0, longitude: place.Longitude ?? 0.0)
                    self.savedPlaces.append(savedPlace)
                }
                
                for place in model?.lstRecentLocations ?? []{
                    let destPlace = SearchPlace(name: place.destionation?.Name ?? "", latitude: place.destionation?.Latitude ?? 0.0, longitude: place.destionation?.Longitude ?? 0.0)
                    self.recentPlaces.append(destPlace)
                    let sourcePlace = SearchPlace(name: place.source?.Name ?? "", latitude: place.source?.Latitude ?? 0.0, longitude: place.source?.Longitude ?? 0.0)
                    self.recentPlaces.append(sourcePlace)
                }
                (self.baseDelegate as? locationSearchDelegate)?.didUpdateLocationData()
            }
        }
    }
}


//MARK:- Google AutoComplete Fetcher Delegate
extension LocationSearchViewModel: GMSAutocompleteFetcherDelegate{
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        searchedPlaces.removeAll()
        for pred in predictions{
            let client = GMSPlacesClient()
            client.fetchPlace(fromPlaceID: pred.placeID, placeFields: GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!, sessionToken: fetcherToken) { (place, error) in
                if let place = place{
                    let searchPlace = SearchPlace(name: place.name ?? "", latitude: Double(place.coordinate.latitude), longitude: Double(place.coordinate.longitude), address: place.formattedAddress ?? "")
                    self.searchedPlaces.append(searchPlace)
                    (self.baseDelegate as? locationSearchDelegate)?.didUpdateLocationData()
                }
            }
        }
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        
    }
}
