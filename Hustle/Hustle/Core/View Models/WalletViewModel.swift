//
//  WalletViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 07/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class WalletViewModel: BaseViewModel {

    private func parametersForWalletService() -> Parameters{
        var params = Dictionary<String, Any>()
        params["customer"] = ["Id": SessionManager.shared.getID()]
        return Parameters(dictionary: params)
    }
    
    func getWalletAmount(completion: @escaping(_ walletAmount: String) -> Void){
        let service = Service(url: ServiceConstants.walletAmountEndoint, parameters: parametersForWalletService(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: WalletAmountDataModel?, _) in
            if let model = model, let amount = model.customer?.customerWallet?.Amount{
                completion("\(amount) SAR")
            }
        }
    }
    
}
