//
//  MapViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 04/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

// this enum is for the current state of map , as we are using same screen for different functions.
enum MapState{
    case start // map is in start state i.e. user has booked a ride yet.
    case estimateFare // user has confirmed the drop off, now he should see the estimate fares.
    case lookingForRide // Ride request is in progress, screen should be showing a loader.
    case waitingForCaptain // Ride is accepted, and user should see the arrival time and location of captain.
    case inRide // Ride is in progress, user should see the current location and estimate arrival time.
}

protocol RideDelegate {
    func didSelectLocations(withDataModel: EstimateFareDataModel)
    func didSendRideRequest()
    func didGetRide()
    func failedToGetRide()
    func rideCancelledByUser()
    func rideCancelledByDriver()
    func driverDidArrive()
    func rideStarted()
    func presentRatingScreen()
    func showError(error: String)
    func showLoading()
    func hideLoading()
    func updateDriverLocation(driverLocation: CLLocationCoordinate2D, destinationLocation: CLLocationCoordinate2D)
    func resetView()
}

class MapViewModel: BaseViewModel {
    
    // identifiers for tracking which location to save
    let pickupLocationIdentifier = "pickupLocationIdentifier"
    let dropLocationIdentifier = "dropLocationIdentifier"
    
    var screenState = MapState.start
    var shouldSelectPickup = false
    var shouldSelectDrop = false
    var pickupLocation: SearchPlace?
    var dropLocation: SearchPlace?
    var pickupCoordinates: CLLocationCoordinate2D?
    var selectedRideId = ""
    var selectedDriverId = ""
    var estimatedValues = Dictionary<String, Any>()
    var dropCoordinates: CLLocationCoordinate2D?
    
    private var estimateFareModel: EstimateFareDataModel?
    private var favouriteDriversList = [DriverListDataModel]()
    
    var notificationDataModel: NotificationDataModel?
    private let scheduler = Scheduler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNotificationObservers()
    }
    
    override func viewWillAppear() {
        if SessionManager.shared.isCurrentTripInProgress(){
            self.getRideDetails()
        }
        getSavedDrivers {
            print("Successfully fetched saved drivers.")
        }
    }
    
    private func addNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(RideAccepted(notification:)), name: .driverAcceptedRide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DriverArrived(notification:)), name: .driverArrived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideCancelledByDriver(notification:)), name: .driverCancelledRide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideStarted(notification:)), name: .rideStarted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideFinished(notification:)), name: .rideEnded, object: nil)
    }
    
    private func updateDriverLocationInVC(){
        (baseDelegate as? RideDelegate)?.updateDriverLocation(driverLocation: CLLocationCoordinate2D(latitude: notificationDataModel?.ride?.driver?.driverLocation?.Latitude ?? 0.0, longitude: notificationDataModel?.ride?.driver?.driverLocation?.Longitude ?? 0.0), destinationLocation: CLLocationCoordinate2D(latitude: notificationDataModel?.ride?.source?.Latitude ?? 0.0, longitude: notificationDataModel?.ride?.source?.Longitude ?? 0.0))
    }
    
    private func updateScheduler(){
        scheduler.setValues(url: ServiceConstants.rideDetailsEndPoint, timeInterval: 7.0, params: parametersForRideDetails().dictionary)
    }
    
    func didTapPickupLocation(){
        self.screenState = .start
        shouldSelectPickup = true
        shouldSelectDrop = false
    }
    
    func didTapDropLocation(){
        self.screenState = .start
        shouldSelectDrop = true
        shouldSelectPickup = false
    }
    
    func setDropLocation(location: SearchPlace){
        self.dropLocation = location
        self.dropCoordinates = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
    }
    
    func setDropCoordinates(coordinates: CLLocationCoordinate2D){
        self.dropCoordinates = coordinates
    }
    
    func setPickupLocation(location: SearchPlace){
        self.pickupLocation = location
        self.pickupCoordinates = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
    }
    
    func setPickupCoordnates(coordinate: CLLocationCoordinate2D){
        self.pickupCoordinates = coordinate
    }
    
    // takes user to initial state
    private func reset(){
        scheduler.setValues(url: "")
        scheduler.stopScheduler()
        self.pickupLocation = nil
        self.dropLocation = nil
        self.screenState = .start
        self.selectedDriverId = ""
        (baseDelegate as? RideDelegate)?.resetView()
    }
    
    func shouldDrawPath() -> Bool{
        return isPickupLocationAvailable() && dropLocation != nil
    }
    
    func isPickupLocationAvailable() -> Bool{
        return (pickupLocation != nil || pickupCoordinates != nil)
    }
    
    func coordinatesForPickupLocation() -> CLLocationCoordinate2D{
        return self.pickupCoordinates ?? CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }
    
    func nameForPickupLocation() -> String{
        if let location = pickupLocation{
            return location.name
        } else if pickupCoordinates != nil{
            return NSLocalizedString("Current Location", comment: "")
        }
        return ""
    }
    
    func addressForPikcupLocation() -> String{
        if let location = pickupLocation, !location.formattedAddress.isEmpty{
            return location.formattedAddress
        }else if let coordinates = pickupCoordinates{
            return "\(coordinates.latitude),\(coordinates.longitude)"
        }
        return ""
    }
    
    func addressForDropLocation() -> String{
        if let location = dropLocation, !location.formattedAddress.isEmpty{
            return location.formattedAddress
        }else if let coordinates = dropCoordinates{
            return "\(coordinates.latitude),\(coordinates.longitude)"
        }
        return ""
    }
    
    func countryCodeForSearch() -> String{
        switch SessionManager.shared.getCurrentCountryCode() {
        case .arab:
            return "sa"
        case .pak:
            return "pk"
        }
    }
    
    func autoCompleteBoundsForSearch() -> GMSCoordinateBounds{
        let lat = LocationManager.shared.lastReceivedLocation()?.latitude ?? 0.0
        let long = LocationManager.shared.lastReceivedLocation()?.longitude ?? 0.0
        let offset = 200.0 / 1000.0;
        let latMax = lat + offset;
        let latMin = lat - offset;
        let lngOffset = offset * cos(lat * Double.pi / 200.0);
        let lngMax = long + lngOffset;
        let lngMin = long - lngOffset;
        let initialLocation = CLLocationCoordinate2D(latitude: latMax, longitude: lngMax)
        let otherLocation = CLLocationCoordinate2D(latitude: latMin, longitude: lngMin)
        return GMSCoordinateBounds(coordinate: initialLocation, coordinate: otherLocation)
    }
    
    func didTapConfirmButton(){
        switch screenState {
        case .start:
            checkAndMoveToEstimateFare()
        case .estimateFare:
            requestRide()
        case .waitingForCaptain, .lookingForRide:
            cancelCurrentRide()
        default:
            break
        }
    }
    
    func didTapCancelButton(){
        self.reset()
    }
    
    func estimatedDriverArrivalTime() -> String{
        return NSLocalizedString("Your driver is arriving in ", comment: "") +  "\(self.notificationDataModel?.DistADurBwCustADvr?.Duration?.Text ?? "some time")"
    }
    
    func carsSelectionData() -> [RideListDataModel]{
        return (self.estimateFareModel?.lstRideType) ?? []
    }
    
    func driverSelectionData() -> [DriverListDataModel]{
        return self.favouriteDriversList
    }
    
    func nameOfDriverAtIndex(index: Int) -> String{
        if index < favouriteDriversList.count{
            return favouriteDriversList[index].getFullName()
        }
        return ""
    }
    
    func idForDriverAtIndex(index: Int) -> String{
        if index < favouriteDriversList.count{
            return favouriteDriversList[index].Id ?? ""
        }
        return ""
    }
    
    //MARK:- Ride Notifications
    @objc private func RideAccepted(notification: Notification){
        if let notificationModel = notification.object as? NotificationDataModel{
            self.notificationDataModel = notificationModel
            self.screenState = .waitingForCaptain
            if let rideID = notificationModel.ride?.Id {
                SessionManager.shared.setCurrentTrip(id: rideID)
                (baseDelegate as? RideDelegate)?.didGetRide()
                updateDriverLocationInVC()
            }
            updateScheduler()
            scheduler.startScheduler { (model: NotificationDataModel?) in
                if let model = model{
                    self.notificationDataModel = model
                    self.updateDriverLocationInVC()
                }
            }
        }
    }
    
    @objc private func DriverArrived(notification: Notification){
        if let notificationModel = notification.object as? NotificationDataModel{
            self.notificationDataModel = notificationModel
            (baseDelegate as? RideDelegate)?.driverDidArrive()
        }
    }
    
    @objc private func RideCancelledByDriver(notification: Notification){
        if let notificationModel = notification.object as? NotificationDataModel{
            self.notificationDataModel = notificationModel
            (baseDelegate as? RideDelegate)?.rideCancelledByDriver()
            self.reset()
        }
    }
    
    @objc private func RideStarted(notification: Notification){
        if let notificationModel = notification.object as? NotificationDataModel{
            self.notificationDataModel = notificationModel
            self.screenState = .inRide
            (baseDelegate as? RideDelegate)?.rideStarted()
            self.reset()
        }
    }
    
    @objc private func RideFinished(notification: Notification){
        if let notificationModel = notification.object as? NotificationDataModel{
            self.notificationDataModel = notificationModel
            (self.baseDelegate as? RideDelegate)?.presentRatingScreen()
            // reset the view , because now user will be taken in ride view controller
            // and this screen will be presented when the ride is finished.
            self.reset()
        }
        
    }
    
    //MARK:- API Parameters
    
    // parameters for ride Details API
    private func parametersForRideDetails() -> Parameters{
        var params = Dictionary<String, Any>()
        params["ride"] = ["Id": SessionManager.shared.getCurrentTripId()]
        return Parameters(dictionary: params)
    }
    
    // parameters for estimate fare API
    private func parametersForEstimateFare() -> Parameters{
        let destinationDictionary: Dictionary<String, Any> = [
            "Id": "",
            "Latitude": String(describing: (dropLocation?.latitude) ?? 0.0),
            "Longitude": String(describing: (dropLocation?.longitude) ?? 0.0),
            "Name": (dropLocation?.name) ?? ""]
        let sourceDictionary: Dictionary<String, Any> = [
            "Id": "",
            "Latitude": String(describing: (self.coordinatesForPickupLocation().latitude)),
            "Longitude": String(describing: (self.coordinatesForPickupLocation().longitude)),
            "Name": (pickupLocation?.name) ?? NSLocalizedString("Current Location", comment: "")]
        let customerDictionary = ["Id": SessionManager.shared.getID()]
        let paramDictionary: Dictionary<String, Any> = ["destination": destinationDictionary, "source": sourceDictionary, "customer": customerDictionary]
        return Parameters(dictionary: paramDictionary)
    }
    
    // parameters for Ride Request API
    private func parametersForRideRequest() -> Parameters{
        var sourceLocationName = ""
        if let pickup = pickupLocation?.name{
            sourceLocationName = pickup
        }
        
        if sourceLocationName.isEmpty{
            sourceLocationName = "Current Location"
        }
        
        let customerDictionary = ["Id": SessionManager.shared.getID()]
        let sourceDictionary = ["Latitude": coordinatesForPickupLocation().latitude,
                                "Longitude": coordinatesForPickupLocation().longitude,
                                "Name": sourceLocationName] as [String : Any]
        let destinationDictionary = ["Latitude": (dropLocation?.latitude) ?? 0.0,
                                     "Longitude": (dropLocation?.longitude) ?? 0.0,
                                     "Name": (dropLocation?.name) ?? ""] as [String: Any]
        let rideTypeCategoryDictionary = ["Id": selectedRideId]
        let driverDictionary = ["Id": selectedDriverId]
        let paramDictionary = ["ride" : ["customer" : customerDictionary,
                                         "source": sourceDictionary,
                                         "destination": destinationDictionary,
                                         "rideTypeCategory": rideTypeCategoryDictionary,
                                         "estimatedFare": estimatedValues] as [String : Any],
                                         "notificationType": 0,
                                         "driver": driverDictionary as [String: Any]] as [String : Any]
        return Parameters(dictionary: paramDictionary)
    }
    
    private func parametersForCancelRide() -> Parameters{
        let params = ["ride" :
            ["id": SessionManager.shared.getCurrentTripId()]
        ]
        return Parameters(dictionary: params)
    }
    
    // parameters for save place
    private func parametersForSavePlace(location: CLLocationCoordinate2D, name: String) -> Parameters{
        var params = Dictionary<String, Any>()
        params["favouritePlace"] = ["customer" : ["Id" : SessionManager.shared.getID()],
                                    "Place":    ["Latitude" : location.latitude,
                                                 "Longitude": location.longitude,
                                                 "Name": name ]]
        return Parameters(dictionary: params)
    }
    
    //MARK:- API Calling
    
    private func getSavedDrivers(success: @escaping() -> Void){
        let url = ServiceConstants.getSavedDriversEndpoint+SessionManager.shared.getID()
        let service = Service(url: url, parameters: Parameters(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: FavouriteDriverDataModel?, error) in
            if let model = model , (model.lstDrivers ?? []).count > 0{
                self.favouriteDriversList = model.lstDrivers!
                success()
            }
        }
    }
    
    func saveLocation(location: CLLocationCoordinate2D, name: String, success: @escaping() -> Void, failure: @escaping() -> Void){
        let service = Service(url: ServiceConstants.savePlaceEndpoint, parameters: parametersForSavePlace(location: location, name: name), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                success()
            }else{
                failure()
            }
        }
    }
    
    private func getRideDetails(){
        let service = Service(url: ServiceConstants.rideDetailsEndPoint, parameters: parametersForRideDetails(), headers: getAPIHeaders())
        (baseDelegate as? RideDelegate)?.showLoading()
        ServiceManager.shared.request(service: service) { (model: NotificationDataModel?, error) in
            (self.baseDelegate as? RideDelegate)?.hideLoading()
            if (model?.ride?.rideStatus?.Title ?? "").lowercased().contains("cancelled"){
                SessionManager.shared.removeCurrentTrip()
            }else {
            
                if let status = RideStatusTypes(rawValue: model?.ride?.rideStatus?.Title ?? ""){
                    self.notificationDataModel = model
                    switch status{
                    case .accepted, .arrived:
                        self.screenState = .waitingForCaptain
                        self.notificationDataModel = model
                        (self.baseDelegate as? RideDelegate)?.didSendRideRequest()
                        (self.baseDelegate as? RideDelegate)?.didGetRide()
                        self.updateDriverLocationInVC()
                        if status == .arrived{
                            (self.baseDelegate as? RideDelegate)?.driverDidArrive()
                        }
                        break
                        
                    case .inRide:
                        (self.baseDelegate as? RideDelegate)?.rideStarted()
                        
                    case .finished, .paid:
                        (self.baseDelegate as? RideDelegate)?.presentRatingScreen()
                        
                    case .requested:
                        self.cancelCurrentRide()
                        
                    default:
                        break
                    }
                }
            }
        }
    }
    
    private func checkAndMoveToEstimateFare(){
        if shouldDrawPath(){
            (baseDelegate as? RideDelegate)?.showLoading()
            let service = Service(url: ServiceConstants.estimateFareEndpoint, parameters: parametersForEstimateFare(), headers: getAPIHeaders())
            ServiceManager.shared.request(service: service) { (model: EstimateFareDataModel?, error) in
                (self.baseDelegate as? RideDelegate)?.hideLoading()
                if let model = model, (model.status ?? "").lowercased().contains("success"){
                    self.screenState = .estimateFare
                    self.estimateFareModel = model
                    (self.baseDelegate as? RideDelegate)?.didSelectLocations(withDataModel: model)
                }else{
                    (self.baseDelegate as? RideDelegate)?.showError(error: NSLocalizedString("Error in fetching estimate fares", comment: ""))
                }
            }
        }else{
            (self.baseDelegate as! RideDelegate).showError(error: NSLocalizedString("Select both pickup and drop location first", comment: ""))
        }
    }
    
    private func requestRide(){
        (baseDelegate as? RideDelegate)?.showLoading()
        let service = Service(url: ServiceConstants.requestRideEndpoint, parameters: parametersForRideRequest(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: RideRequestDataModel?, error) in
            if ((model?.status) ?? "").lowercased().contains("success"){
                if let rideID = model?.ride?.Id{
                    SessionManager.shared.setCurrentTrip(id: rideID)
                    self.screenState = .lookingForRide
                    (self.baseDelegate as? RideDelegate)?.didSendRideRequest()
                }
            }else{
                (self.baseDelegate as? RideDelegate)?.hideLoading()
                (self.baseDelegate as? RideDelegate)?.showError(error: model?.status ?? "")
            }
        }
    }
    
    private func cancelCurrentRide(){
        (baseDelegate as? RideDelegate)?.showLoading()
        let service = Service(url: ServiceConstants.cancelCurrentRideEndpoint, parameters: parametersForCancelRide(), headers: getAPIHeaders())
        
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, _) in
            (self.baseDelegate as? RideDelegate)?.hideLoading()
            if (model?.status ?? "").lowercased().contains("success"){
                self.screenState = .start
                SessionManager.shared.removeCurrentTrip()
                (self.baseDelegate as? RideDelegate)?.rideCancelledByUser()
                self.reset() // reset everything to initial state
            }
        }
    }
}
