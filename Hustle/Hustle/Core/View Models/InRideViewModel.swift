//
//  InRideViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 03/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GooglePlaces

protocol InRideDelegate {
    func updateData()
    func rideEnded()
    func updateDriverLocation(driverLocation: CLLocationCoordinate2D?, dropLocation: CLLocationCoordinate2D?)
}

class InRideViewModel: BaseViewModel {

    private var dataModel = NotificationDataModel()
    private let scheduler = Scheduler(url: ServiceConstants.rideDetailsEndPoint,timeInterval: 7.0,
                              params: ["ride":["Id": SessionManager.shared.getCurrentTripId()]])
    
    override func viewWillAppear() {
        super.viewWillAppear()
        scheduler.startScheduler { (model: NotificationDataModel?) in
            if let model = model{
                self.dataModel = model
                self.checkForRideEnd()
                (self.baseDelegate as? InRideDelegate)?.updateData()
                self.updateDriverLocation()
            }
        }
    }
    
    func setDataModel(_ model: NotificationDataModel){
        self.dataModel = model
    }
    
    private func checkForRideEnd(){
        if ["finished","paid"].contains((dataModel.ride?.rideStatus?.Title ?? "").lowercased()){
            (self.baseDelegate as? InRideDelegate)?.rideEnded()
        }
    }
    
    private func updateDriverLocation(){
        (self.baseDelegate as? InRideDelegate)?.updateDriverLocation(driverLocation: dataModel.ride?.driver?.driverLocation?.getCoordinates(), dropLocation: dataModel.ride?.destination?.getCoordinates())
    }
    
    @objc private func rideEnded(){
        (self.baseDelegate as? InRideDelegate)?.rideEnded()
    }
    
    override func viewWillDissappear() {
        scheduler.stopScheduler()
    }
    
    func reminingTimeInRide() -> String{
        return dataModel.DistADurBwDvrADest?.Duration?.Text ?? ""
    }
    
    func remainingDistanceInRide() -> String{
        return dataModel.DistADurBwDvrADest?.Distance?.Text ?? ""
    }
    
    func nameOfDriver() -> String{
        return dataModel.ride?.driver?.getFullName() ?? ""
    }
    
    func ratingOfDriver() -> String{
        return dataModel.ride?.driver?.getRatingText() ?? ""
    }
    
    func completedTripsOfDriver() -> String{
        return dataModel.ride?.driver?.getCompletedTripsText() ?? ""
    }
    
    func pickupLocationName() -> String{
        return dataModel.ride?.source?.Name ?? ""
    }
    
    func pickupLocationAddress() -> String{
        return ""
    }
    
    func dropLocationName() -> String{
        return dataModel.ride?.destination?.Name ?? ""
    }
    
    func dropLocationAddress() -> String{
        return ""
    }
    
    func estimateFare() -> String{
        return dataModel.ride?.estimatedFare?.getEstimatedCost() ?? ""
    }
    
    //MARK:- Service Calling
    private func parametersForSaveDriver() -> Parameters{
        var params = Dictionary<String, Any>()
        params["favouritedriver"] =  ["customer": ["Id": SessionManager.shared.getID()],
                                           "driver": ["Id": dataModel.ride?.driver?.Id ?? ""],
                                           "rideTypeCategory": ["Id": dataModel.ride?.rideTypeCategory?.Id ?? ""]]
        return Parameters(dictionary: params)
    }
    
    func saveDriver(success: @escaping () -> Void, failure: @escaping () -> Void){
        let service = Service(url: ServiceConstants.saveDriverEndpoint, parameters: parametersForSaveDriver(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                success()
            }else{
                failure()
            }
        }
    }
}
