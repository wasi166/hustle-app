//
//  RateDriverViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 28/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class RateDriverViewModel: BaseViewModel {

    var dataModel: NotificationDataModel?
    
    func fareForRide() -> String?{
        if let fare = dataModel?.ride?.rideFare?.TotalFare{
            return "\(fare) SAR"
        }
        return "100.0 SAR"
    }
    
    func getDriverFullName() -> String?{
        return dataModel?.ride?.driver?.getFullName()
    }
    
    func getDriverRating() -> String?{
        return dataModel?.ride?.driver?.getRatingText()
    }
    
    func getDriverTotalTrips() -> String?{
        return dataModel?.ride?.driver?.getCompletedTripsText()
    }
    
    func errorForInvalidRating() -> String{
        return NSLocalizedString("Rating should be from 1 to 5 stars", comment: "")
    }
    
    func errorForFailedSendRating() -> String{
        return NSLocalizedString("There was some error in sending rating. Try again later", comment: "")
    }
    
    private func parametersForRateDriver(rating: Int) -> Parameters{
        var params = Dictionary<String, Any>()
        params["feedback"] = ["ride":["Id":SessionManager.shared.getCurrentTripId()],
                              "CompplimentText": "",
                              "TotalStars": String(describing: rating)]
        return Parameters(dictionary: params)
    }
    
    func sendRate(rating: Int, completion: @escaping(_ success: Bool) -> Void){
        let service = Service(url: ServiceConstants.rateDriverEndpoint, parameters: parametersForRateDriver(rating: rating), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, error) in
            if let model = model, (model.status ?? "").lowercased().contains("success"){
                completion(true)
            }else{
                completion(false)
            }
        }
    }
}
