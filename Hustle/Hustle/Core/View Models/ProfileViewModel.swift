//
//  ProfileViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 08/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class ProfileViewModel: BaseViewModel {

    func getUserData() -> [String]{
        return [firstName(), getLastName() , email() , phoneNumber()]
    }
    
    func firstName() -> String{
        return SessionManager.shared.getFirstName()
    }
    
    func getLastName() -> String{
        return SessionManager.shared.getLastName()
    }
    
    func email() -> String{
        return SessionManager.shared.getEmail()
    }
    
    func phoneNumber() -> String{
        return SessionManager.shared.getPhoneNumber()
    }
    
    private func parametersForSaveProfile(userData: [String]) -> Parameters{
        var paramsDictionary = Dictionary<String, Any>()
        var customerDictionary = Dictionary<String, Any>()
        customerDictionary["Id"] = SessionManager.shared.getID()
        let keys = ["FirstName", "LastName", "EmailAddress", "PhoneNumber"]
        for i in 0..<userData.count{
            customerDictionary[keys[i]] = userData[i]
        }
        customerDictionary["Image"] = "null"
        paramsDictionary["customer"] = customerDictionary
        return Parameters(dictionary: paramsDictionary)
    }
    
    func saveProfile(userData: [String], success: @escaping() -> Void, failure: @escaping(String) -> Void){
        let service = Service(url: ServiceConstants.saveProfileEndpoint, parameters: parametersForSaveProfile(userData: userData), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: UpdateProfileDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("succes"){
                if let customer = model?.customer{
                    SessionManager.shared.updateProfile(accountInfo: customer)
                    success()
                }
            }else{
                failure(NSLocalizedString("Error in saving Profile", comment: ""))
            }
        }
    }
}
