//
//  SignupViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 29/06/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

protocol SignupViewDelegate {
    func didSignupSuccessfully()
    func didFailToSignup(error: String)
}

class SignupViewModel: BaseViewModel {

    func isFormValid(name: String, email: String, password: String, confirmPassword: String) -> Bool{
        return !(name.isEmpty || email.isEmpty || password.isEmpty || confirmPassword.isEmpty)
    }
    
    func errorForForm(name: String, email: String, password: String, confirmPassword: String) -> String{
        return NSLocalizedString("Please Fill All the fields", comment: "")
    }

    private func currentCountryCode() -> CountryCodeType{
        return SessionManager.shared.getCurrentCountryCode()
    }
    
    private func phoneNumber(phone: String) -> String{
        var phoneNumber = phone
        if !(phoneNumber.hasPrefix(self.currentCountryCode().countryCodeString())){
            if phoneNumber.hasPrefix("0"){
                phoneNumber = String(phoneNumber.dropFirst())
            }
            phoneNumber = self.currentCountryCode().countryCodeString() + phoneNumber
        }
        return phoneNumber
    }
    
    func signUp(firstName: String, lastName: String, email: String, phone: String, otp: String){
        let parameters = Parameters(dictionary: getParameters(firstName: firstName, lastName: lastName, email: email, phone: phone, otp: otp))
        let service = Service(url: ServiceConstants.registerEndpoint, parameters: parameters)
        ServiceManager.shared.request(service: service) { (model: ValidateNumberDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success") || model?.customerData != nil{
                SessionManager.shared.loginWith(AccountInfo: (model?.customerData)!, token: (model?.token) ?? "")
                (self.baseDelegate as? SignupViewDelegate)?.didSignupSuccessfully()
            }else{
                (self.baseDelegate as? SignupViewDelegate)?.didFailToSignup(error: NSLocalizedString("Error in signing up", comment: ""))
            }
        }
    }
    
    private func getParameters(firstName: String, lastName: String, email: String, phone: String, otp: String) -> [String: Any]{
        var params = [String: Any]()
        params["CustomerData"] = ["FirstName": firstName,
                                  "LastName": lastName,
                                  "EmailAddress": email,
                                  "PhoneNumber": self.phoneNumber(phone: phone),
                                  "FireBaseToken": SessionManager.shared.getFCMToken()]
        params["OTP"] = otp
        return params
    }
}
