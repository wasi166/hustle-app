//
//  SelectionViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 15/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

enum SelectionType{
    case car
    case driver
}

class SelectionViewModel: BaseViewModel {

    var selectionType = SelectionType.car
    private var carDataArray = [RideListDataModel]()
    private var driverDataArray = [DriverListDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setDataArray(dataArray: [Any]){
        if selectionType == .car, let data = dataArray as? [RideListDataModel]{
            carDataArray = data
        }else if let data = dataArray as? [DriverListDataModel]{
            driverDataArray = data
        }
    }
    
    func numberOfSections() -> Int{
        if selectionType == .car{
            return self.carDataArray.count
        }else {
            return 1
        }
    }
    
    func numberOfRowsInSection(section: Int) -> Int{
        if selectionType == .car{
            return (self.carDataArray[section].lstCategory?.count) ?? 0
        }else{
            return driverDataArray.count
        }
    }
    
    func dataForIndexPath(indexPath: IndexPath) -> Dictionary<String, Any>{
        var data = Dictionary<String, Any>()
        if self.selectionType == .car{
            let rideData = self.carDataArray[indexPath.section].lstCategory![indexPath.row]
            data[BaseCellConstant.Identifier] = CarSelectionTableViewCell.identifier
            data["name"] = rideData.Title ?? NSLocalizedString("Unknown", comment: "")
            data["logo"] = rideData.Logo ?? NSLocalizedString("Unknown", comment: "")
        }else if self.selectionType == .driver{
            let driverData = self.driverDataArray[indexPath.row]
            data[BaseCellConstant.Identifier] = DriverSelectionTableViewCell.identifier
            data[DriverSelectionTableViewCell.nameKey] = driverData.getFullName()
            data[DriverSelectionTableViewCell.ratingKey] = driverData.getRatingText()
            data[DriverSelectionTableViewCell.tripsKey] = driverData.getCompletedTripsText()
        }
        return data
    }
//
//    func heightForHeaderInSection() -> Int{
//        return 20
//    }
    
    func dataForSectionHeader(section: Int) -> Dictionary<String, Any>{
        var data = Dictionary<String, Any>()
        data[BaseCellConstant.Identifier] = SelectionHeaderTableViewCell.identifier
        if selectionType == .car{
            data["name"] = self.carDataArray[section].Title
        }else{
            data["name"] = "Saved Drivers"
        }
        return data
    }
    
}
