//
//  RideHistoryTableViewCell.swift
//  Hustle
//
//  Created by  Wasi Tariq on 10/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class RideHistoryTableViewCell: BaseTableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var fareButton: UIButton!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var distanceButton: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func initializeUI() {
        super.initializeUI()
        // date label
        [dateLabel, timeLabel].forEach { (label) in
            label?.textColor = AppTheme.colorSecondary
            label?.font = AppTheme.fontText(ofSize: 14.0)
        }
        // fare button
        fareButton.setTitleColor(AppTheme.colorSecondary, for: .normal)
        fareButton.tintColor = AppTheme.colorSecondary
        fareButton.titleLabel?.font = AppTheme.fontText(ofSize: 14.0)
        // source label
        [sourceLabel, destinationLabel].forEach { (label) in
            label?.textColor = AppTheme.colorText
            label?.font = AppTheme.fontText(ofSize: 14.0)
        }
        // distance button
        distanceButton.setTitleColor(AppTheme.colorText, for: .normal)
        distanceButton.tintColor = AppTheme.colorText
        distanceButton.titleLabel?.font = AppTheme.fontText(ofSize: 14.0)
    }
    
    override func configure(data: Any, indexPath: IndexPath) {
        if let data = data as? Dictionary<String, Any>,
            let model = data["data"] as? RideHistoryDataModel{
            self.sourceLabel.text = model.source?.Name ?? ""
            self.destinationLabel.text = model.destination?.Name ?? ""
            self.dateLabel.text = model.Date ?? ""
            self.timeLabel.text = model.Time ?? ""
            self.fareButton.setTitle("\(String(describing:model.rideFare?.TotalFare ?? 0.0)) SR", for: .normal)
        }
    }
}
