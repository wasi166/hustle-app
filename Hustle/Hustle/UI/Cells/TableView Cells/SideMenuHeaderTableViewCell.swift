//
//  SideMenuHeaderTableViewCell.swift
//  Hustle
//
//  Created by  Wasi Tariq on 14/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

protocol SideMenuHeaderDelegate {
    func presentEditProfile()
}

class SideMenuHeaderTableViewCell: BaseTableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editProfileButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        setValues()
    }

    override func initializeUI() {
        self.contentView.backgroundColor = AppTheme.colorDark
        
        self.profileImageView.layer.cornerRadius = profileImageView.bounds.width/2
        self.profileImageView.clipsToBounds = true
        
        self.nameLabel.font = AppTheme.fontText(ofSize: 14.0)
        self.nameLabel.textColor = .white
        
        self.editProfileButton.setTitleColor(AppTheme.colorSecondary, for: .normal)
        self.editProfileButton.titleLabel?.font = AppTheme.fontText(ofSize: 14.0)
    }

    private func setValues(){
        self.nameLabel.text = SessionManager.shared.getFullName()
    }
    
    @IBAction func didTapEditProfileButton(_ sender: Any) {
        (self.baseDelegate as? SideMenuHeaderDelegate)?.presentEditProfile()
    }
}
