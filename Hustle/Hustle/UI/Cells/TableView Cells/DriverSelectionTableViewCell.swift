//
//  DriverSelectionTableViewCell.swift
//  Hustle
//
//  Created by  Wasi Tariq on 15/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class DriverSelectionTableViewCell: BaseTableViewCell {

    static let nameKey = "NameKey"
    static let ratingKey = "RatingKey"
    static let tripsKey = "TripsKey"
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var tripsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func initializeUI() {
        super.initializeUI()
        
        profileImageView.layer.cornerRadius = profileImageView.bounds.width/2
        profileImageView.clipsToBounds = true
        profileImageView.layer.borderWidth = 0.5
        profileImageView.layer.borderColor = AppTheme.colorPrimary.cgColor
        
        [nameLabel, ratingLabel].forEach { (label) in
            label?.textColor = AppTheme.colorSecondary
            label?.font = AppTheme.fontText(ofSize: 14.0)
        }
        
        tripsLabel.font = AppTheme.fontText(ofSize: 12.0)
        tripsLabel.textColor = AppTheme.colorSecondary
    }
    
    override func configure(data: Any, indexPath: IndexPath) {
        if let data = data as? Dictionary<String, Any>{
            nameLabel.text = data[DriverSelectionTableViewCell.nameKey] as? String
            ratingLabel.text = data[DriverSelectionTableViewCell.ratingKey] as? String
            tripsLabel.text = data[DriverSelectionTableViewCell.tripsKey] as? String
        }
    }
}
