//
//  LocationSearchTableViewCell.swift
//  Hustle
//
//  Created by  Wasi Tariq on 21/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class LocationSearchTableViewCell: BaseTableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func initializeUI() {
        super.initializeUI()
        // title label
        titleLabel.textColor = .black
        titleLabel.font = AppTheme.fontText(ofSize: 16.0)
        // address label
        addressLabel.textColor = AppTheme.colorText
        addressLabel.font = AppTheme.fontText(ofSize: 13.0)
        
    }
    
    override func configure(data: Any, indexPath: IndexPath) {
        if let place = data as? SearchPlace{
            titleLabel.text = place.name
            addressLabel.text = place.formattedAddress
            
            if place.name.isEmpty{
                titleLabel.text = "Unnamed Location"
            }
            
            if place.formattedAddress.isEmpty{
                addressLabel.text = "\(place.latitude),\(place.longitude)"
            }
        }
    }
    
}
