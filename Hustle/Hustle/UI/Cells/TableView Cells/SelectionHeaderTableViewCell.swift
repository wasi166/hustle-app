//
//  SelectionHeaderTableViewCell.swift
//  Hustle
//
//  Created by  Wasi Tariq on 16/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class SelectionHeaderTableViewCell: BaseTableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func initializeUI() {
        super.initializeUI()
        
        self.nameLabel.textColor = AppTheme.colorText
        self.nameLabel.font = AppTheme.fontHeading(ofSize: 13.0)
    }
    
    override func configure(data: Any, indexPath: IndexPath) {
        if let data = data as? Dictionary<String, Any>{
            self.nameLabel.text = data["name"] as? String
        }
    }
}
