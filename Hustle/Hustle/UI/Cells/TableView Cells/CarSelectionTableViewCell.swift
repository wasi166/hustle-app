//
//  CarSelectionTableViewCell.swift
//  Hustle
//
//  Created by  Wasi Tariq on 15/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//


import UIKit

class CarSelectionTableViewCell: BaseTableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func initializeUI() {
        super.initializeUI()
        
        nameLabel.textColor = AppTheme.colorText
        nameLabel.font = AppTheme.fontHeading(ofSize: 13.0)
    }
    
    override func configure(data: Any, indexPath: IndexPath) {
        if let data = data as? Dictionary<String , Any>{
            self.nameLabel.text = data["name"] as? String
        }
    }
    
}
