//
//  MapViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 04/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SideMenu
import EasyPopUp

class MapViewController: BaseViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var currentLocationButton: UIButton!
    
    @IBOutlet weak var LocationsContainerView: UIView!
    @IBOutlet weak var pickupLocationView: UIView!
    @IBOutlet weak var pickupLocationTitleLabel: UILabel!
    @IBOutlet weak var pickupLocationDescLabel: UILabel!
    @IBOutlet weak var pickupLocationFavouriteButton: UIButton!
    @IBOutlet weak var dropLocationTitleLabel: UILabel!
    @IBOutlet weak var DropLocationDescLabel: UILabel!
    @IBOutlet weak var dropLocationFavouriteButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    private let viewModel = MapViewModel()
    private let location = LocationManager.shared
    
    private let statusLabel = UILabel()
    private var pickupMarker = GMSMarker()
    private var dropMarker = GMSMarker()
    private var polyline = GMSPolyline()
    private var estimateFareView: EstimateFareView?
    private var driverInfoView: DriverInfoView?
    private var driverMarker = GMSMarker()
    private var userSelectedMarker = GMSMarker()
    private var didPickupMarkerAddedOnCurrentLocation = false // this is to solve bug where multiple markers were adding on tapping current location button
    private var popup: EasyPopup?
    
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        viewModel.baseDelegate = self
    }
    
    override func configureUI() {
        super.configureUI()
        configureMapView()
        configureEstimateFareView()
        configureDriverInfoView()
        configureStatusView()
        // confirm Button
        confirmButton.backgroundColor = AppTheme.colorPrimary
        confirmButton.setTitleColor(.white, for: .normal)
        confirmButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
        confirmButton.layer.cornerRadius = 10.0
        
        // current location button
        currentLocationButton.backgroundColor = .white
        currentLocationButton.layer.cornerRadius = 10.0
        
        // pickup location title label
        pickupLocationTitleLabel.font = AppTheme.fontText(ofSize: 14.0)
        
        // pickup location desc label
        pickupLocationDescLabel.font = AppTheme.fontText(ofSize: 12.0)
        pickupLocationDescLabel.textColor = AppTheme.colorText
        
        // drop location title label
        dropLocationTitleLabel.font = AppTheme.fontText(ofSize: 14.0)
        
        // drop location desc label
        DropLocationDescLabel.textColor = AppTheme.colorText
        DropLocationDescLabel.font = AppTheme.fontText(ofSize: 12.0)
        
        // cancel button
        cancelButton.setTitleColor(.white, for: .normal)
        cancelButton.backgroundColor = AppTheme.colorSecondary
        cancelButton.layer.cornerRadius = 10.0
        cancelButton.titleLabel?.font = AppTheme.fontText(ofSize: 16.0)
        cancelButton.isHidden = true
        
        // location container view & pickup location view
        //FIXME: Text field is using same code , why not make a UIView extension??? 🙄
        [LocationsContainerView, pickupLocationView].forEach { (view) in
            view?.layer.cornerRadius = 10.0
            view?.layer.masksToBounds = false
            view?.layer.shadowRadius = 0.3
            view?.layer.shadowColor = UIColor.black.cgColor
            view?.layer.shadowOffset = CGSize(width: 0, height: 0.5)
            view?.layer.shadowOpacity = 0.3
        }
        
        //FIXME: what the hell is this line??? 😡
        for subview in self.mapView.subviews where ((subview is UIButton) || (subview is UITextField)){
            mapView.bringSubviewToFront(subview)
            print(subview)
        }
        mapView.bringSubviewToFront(LocationsContainerView)
    }
    
    private func drawMarkerIfAvailable(){
        if viewModel.isPickupLocationAvailable(){
            addPickupMarker(position: viewModel.coordinatesForPickupLocation(), name: viewModel.nameForPickupLocation(), address: viewModel.addressForPikcupLocation())
        }
        
        if let drop = viewModel.dropLocation, let dropCoordinates = viewModel.dropCoordinates{
            dropMarker = GMSMarker()
            dropMarker.isDraggable = true
            dropMarker.icon = #imageLiteral(resourceName: "drop-marker")
            dropMarker.map = self.mapView
            dropMarker.position = dropCoordinates
            dropMarker.title = "Drag and Drop for location change"
            mapView.selectedMarker = dropMarker
            dropLocationTitleLabel.text = drop.name
            DropLocationDescLabel.text = viewModel.addressForDropLocation()
        }
        if viewModel.shouldDrawPath(){
            self.cancelButton.isHidden = false
            self.drawPolygon(from: viewModel.coordinatesForPickupLocation(), to: viewModel.dropCoordinates!)
        }
    }
    
    private func addPickupMarkerToCurrentLocation(){
        if let location = LocationManager.shared.lastReceivedLocation(){
            viewModel.setPickupCoordnates(coordinate: location)
            addPickupMarker(position: location, name: NSLocalizedString("Current Location", comment: ""), address: "\(location.latitude),\(location.longitude)")
            self.didPickupMarkerAddedOnCurrentLocation = true
        }
    }
    
    private func addPickupMarker(position: CLLocationCoordinate2D, name: String, address: String){
        pickupMarker = GMSMarker()
        pickupMarker.isDraggable = true
        pickupMarker.map = self.mapView
        pickupMarker.icon = #imageLiteral(resourceName: "pickup-marker")
        pickupMarker.position = position
        pickupMarker.title = "Drag and drop for location change"
        pickupLocationTitleLabel.text = name
        pickupLocationDescLabel.text = address
        self.mapView.selectedMarker = pickupMarker
    }
    
    private func configureMapView(){
        mapView.isMyLocationEnabled = true
        self.mapView.delegate = self
        // pickup marker
        pickupMarker.map = self.mapView
        pickupMarker.icon = #imageLiteral(resourceName: "pickup-marker")
        
        // drop marker
        dropMarker.icon = #imageLiteral(resourceName: "drop-marker")
        dropMarker.map = self.mapView
        
        navigateToCurrentLocation()
        addLocationNotificationObserver()
    }
    
    private func addLocationNotificationObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToCurrentLocation), name: .receivedLocationFirstTime, object: nil)
    }
    
    private func resetLocationViews(){
        LocationsContainerView.alpha = 1.0
        pickupLocationTitleLabel.text = NSLocalizedString("Pickup Location", comment: "")
        pickupLocationDescLabel.text = NSLocalizedString("Select Pickup Location", comment: "")
        dropLocationTitleLabel.text = NSLocalizedString("Drop Location", comment: "")
        DropLocationDescLabel.text = NSLocalizedString("Select Drop Location", comment: "")
    }
    
    private func configureEstimateFareView(){
        estimateFareView = Bundle.main.loadNibNamed(EstimateFareView.nibName, owner: self, options: nil)![0] as? EstimateFareView
        self.mapView.addSubview(estimateFareView!)
        estimateFareView!.translatesAutoresizingMaskIntoConstraints = false
        let bottomConstraint = NSLayoutConstraint(item: estimateFareView!, attribute: .bottom, relatedBy: .equal, toItem: confirmButton, attribute: .top, multiplier: 1.0, constant: -5.0)
        let centerConstraint = NSLayoutConstraint(item: estimateFareView!, attribute: .centerX, relatedBy: .equal, toItem: self.mapView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        NSLayoutConstraint.activate([bottomConstraint, centerConstraint])
        self.mapView.addConstraints([bottomConstraint, centerConstraint])
        estimateFareView?.heightAnchor.constraint(equalToConstant: CGFloat(EstimateFareView.height)).isActive = true
        estimateFareView?.widthAnchor.constraint(equalToConstant: CGFloat(EstimateFareView.width)).isActive = true
        estimateFareView?.alpha = 0.0
        estimateFareView?.layer.cornerRadius = 10.0
        estimateFareView?.clipsToBounds = true
        estimateFareView?.delegate = self
        self.mapView.bringSubviewToFront(estimateFareView!)
    }
    
    private func configureDriverInfoView(){
        driverInfoView = Bundle.main.loadNibNamed(DriverInfoView.nibName, owner: self, options: nil)![0] as? DriverInfoView
        self.mapView.addSubview(driverInfoView!)
        driverInfoView?.translatesAutoresizingMaskIntoConstraints = false
        let bottomConstraint = NSLayoutConstraint(item: driverInfoView!, attribute: .bottom, relatedBy: .equal, toItem: confirmButton, attribute: .top, multiplier: 1.0, constant: -5.0)
        let centerConstraint = NSLayoutConstraint(item: driverInfoView!, attribute: .centerX, relatedBy: .equal, toItem: self.mapView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        NSLayoutConstraint.activate([bottomConstraint, centerConstraint])
        self.mapView.addConstraints([bottomConstraint, centerConstraint])
        driverInfoView?.heightAnchor.constraint(equalToConstant: CGFloat(DriverInfoView.height)).isActive = true
        driverInfoView?.widthAnchor.constraint(equalToConstant: CGFloat(DriverInfoView.width)).isActive = true
        driverInfoView?.alpha = 0.0
        driverInfoView?.layer.cornerRadius = 10.0
        driverInfoView?.clipsToBounds = true
        self.mapView.bringSubviewToFront(driverInfoView!)
    }
    
    private func configureStatusView(){
        statusLabel.font = AppTheme.fontHeading(ofSize: 14.0)
        statusLabel.textColor = .black
        statusLabel.backgroundColor = UIColor(white: 1, alpha: 0.6)
        statusLabel.textAlignment = .center
        statusLabel.clipsToBounds = true
        statusLabel.layer.cornerRadius = 8
        hideStatusView()
        self.mapView.addSubview(statusLabel)
        mapView.bringSubviewToFront(statusLabel)
    }
    
    private func hideStatusView(){
        statusLabel.alpha = 0.0
    }
    
    private func showStatusView(with rect: CGRect){
        statusLabel.frame = rect
        statusLabel.alpha = 1.0
    }
    
    private func hideLocationInputs(){
        self.LocationsContainerView.alpha = 0.0
    }
    
    private func showLLocationInputs(){
        self.LocationsContainerView.alpha = 1.0
    }

    private func showEstimateFare(){
        estimateFareView!.transform = CGAffineTransform(translationX: 0, y: 20)
        UIView.animate(withDuration: 0.5) {
            self.estimateFareView?.transform = .identity
            self.estimateFareView?.alpha = 1.0
        }
    }
    
    private func hideEstimateFareView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.estimateFareView?.transform = CGAffineTransform(translationX: 0, y: -20)
            self.estimateFareView?.alpha = 0.0
        }) { (_) in
            self.estimateFareView?.transform = .identity
            self.estimateFareView?.setDriverName(name: "Random Driver")
        }
    }
    
    private func showDriverInfoView(){
        driverInfoView!.transform = CGAffineTransform(translationX: 0, y: 20)
        driverInfoView?.populateData(with: viewModel.notificationDataModel?.ride?.driver)
        UIView.animate(withDuration: 0.5) {
            self.driverInfoView?.transform = .identity
            self.driverInfoView?.alpha = 1.0
        }
    }
    
    private func hideDriverInfoView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.driverInfoView?.transform = CGAffineTransform(translationX: 0, y: -20)
            self.driverInfoView?.alpha = 0.0
        }) { (_) in
            self.driverInfoView?.transform = .identity
        }
    }
    
    private func presentAutoCompleteViewController(){
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.presentLocationSearchFromMap, sender: self)
    }
    
    private func drawDriverMarker(){
        driverMarker.icon = #imageLiteral(resourceName: "driver-marker")
        driverMarker.map = self.mapView
    }
    
    private func drawUserSelectedMarker(){
        userSelectedMarker.icon = #imageLiteral(resourceName: "pickup-marker")
        userSelectedMarker.map = self.mapView
    }
    
    private func presentSavePlaceView(withIdentifier: String){
        let savePlaceView = Bundle.main.loadNibNamed(SavePlaceView.nibName, owner: self, options: [:])![0] as! SavePlaceView
        savePlaceView.delegate = self
        savePlaceView.identifier = withIdentifier
        popup = EasyPopup(superView: self.view, viewTopop: savePlaceView)
        popup?.showPopup()
    }
    
    @objc private func navigateToCurrentLocation(){
        if let currentLocation = location.lastReceivedLocation(){
            self.mapView.animate(to: GMSCameraPosition(latitude: currentLocation.latitude, longitude: currentLocation.longitude, zoom: 15.0))
            // removing the current location blue icon of google maps
            self.mapView.isMyLocationEnabled = false
            // adding pickup marker to current location if pikcup location is not set yet
            if viewModel.pickupLocation == nil && !self.didPickupMarkerAddedOnCurrentLocation{
                addPickupMarkerToCurrentLocation()
            }
            // removing observer because it has got the user location.
            // why removing?
            // Well i guess i can save memory by doing that 😇 , maybe 🙄
            NotificationCenter.default.removeObserver(self, name: .receivedLocationFirstTime, object: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.Segues.Ride.presentSelectionFromMap{
            if let data = sender as? Dictionary<String, Any>{
                let destination = segue.destination as! SelectionViewController
                destination.selectionType = data["type"] as!
                SelectionType
                destination.delegate = self
            }
        } else if segue.identifier == UIConstants.Segues.Ride.presentRateDriverFromMap{
            let destination = segue.destination as! RateDriverViewController
            destination.dataModel = self.viewModel.notificationDataModel
        } else if segue.identifier == UIConstants.Segues.Ride.showInRideFromMap{
            let destination = segue.destination as! InRideViewController
            destination.dataModel = self.viewModel.notificationDataModel
        } else if segue.identifier == UIConstants.Segues.Ride.presentLocationSearchFromMap{
            let destination = (segue.destination as! UINavigationController).viewControllers[0] as! LocationSearchViewController
            destination.delegate = self
        }
    }
    
    //MARK:- IBActions
    @IBAction func didTapConfirmButton(_ sender: Any) {
        viewModel.didTapConfirmButton()
    }
    
    @IBAction func didTapCurrentLocationButton(_ sender: Any) {
        navigateToCurrentLocation()
    }
    
    @IBAction func didTapSideMenuButton(_ sender: Any) {
        performSegue(withIdentifier: UIConstants.Segues.Ride.presentSideMenu, sender: self)
    }
    
    @IBAction func didTapMarkFavoritePickupLocation(_ sender: Any) {
        presentSavePlaceView(withIdentifier: viewModel.pickupLocationIdentifier)
    }
    
    @IBAction func didTapFavouriteDropLocation(_ sender: Any) {
        if let _ = viewModel.dropCoordinates{
            presentSavePlaceView(withIdentifier: viewModel.dropLocationIdentifier)
        }else{
            self.showMessage(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Location is Empty", comment: ""), type: .error)
        }
    }
    
    @IBAction func didTapPickupLocation(){
        hideEstimateFareView()
        viewModel.didTapPickupLocation()
        presentAutoCompleteViewController()
    }
    
    @IBAction func didTapDropLocation(){
        hideEstimateFareView()
        viewModel.didTapDropLocation()
        presentAutoCompleteViewController()
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        viewModel.didTapCancelButton()
    }
}

//MARK:- LocationSearch delegate
extension MapViewController: LocationSearchControllerDelegate{
    func didCompleteWithPlace(place: SearchPlace) {
        if viewModel.shouldSelectPickup{
            // dont allow the same pickup and drop off locations
            if viewModel.dropCoordinates?.longitude ?? 0 == place.longitude && viewModel.dropCoordinates?.latitude ?? 0 == place.latitude{
                self.showMessage(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Cannot select same pickup and drop location", comment: ""), type: .error)
                return
            }
            viewModel.setPickupLocation(location: place)
        }else{
            if viewModel.pickupCoordinates?.longitude ?? 0 == place.longitude && viewModel.pickupCoordinates?.latitude ?? 0 == place.latitude{
                self.showMessage(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Cannot select same pickup and drop location", comment: ""), type: .error)
                return
            }
            viewModel.setDropLocation(location: place)
        }
        self.mapView.clear()
        drawMarkerIfAvailable()
        self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude))
    }
}

// MARK:- RIDE DELEGATE
extension MapViewController: RideDelegate{
    func didSelectLocations(withDataModel: EstimateFareDataModel) {
        self.currentLocationButton.alpha = 0.0
        estimateFareView?.dataModel = withDataModel
        showEstimateFare()
        // by default select the first available option
        self.didSelctItem(at: IndexPath(row: 0, section: 0), selectionType: .car)
        self.cancelButton.isHidden = false
    }
    
    func didSendRideRequest() {
        cancelButton.isHidden = true
        statusLabel.text = NSLocalizedString("Connecting you with nearby driver", comment: "")
        self.showStatusView(with: CGRect(x: mapView.center.x - 125, y: LocationsContainerView.frame.origin.y + LocationsContainerView.bounds.height, width: 250, height: 40))
        self.confirmButton.setTitle(NSLocalizedString("Cancel Trip", comment: ""), for: .normal)
        self.confirmButton.backgroundColor = AppTheme.colorSecondary
        hideLocationInputs()
    }
    
    func didGetRide() {
        statusLabel.text = viewModel.estimatedDriverArrivalTime()
        showStatusView(with: CGRect(x: mapView.center.x - 125, y: confirmButton.frame.origin.y - 180, width: 250, height: 40))
        self.hideLoader()
        hideEstimateFareView()
        showDriverInfoView()
    }
    
    func rideStarted() {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.showInRideFromMap, sender: nil)
    }
    
    func presentRatingScreen() {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.presentRateDriverFromMap, sender: nil)
    }
    
    func failedToGetRide() {
        
    }
    
    func rideCancelledByUser() {
        self.showMessage(title: NSLocalizedString("Ride Cancelled", comment: ""), message: NSLocalizedString("Ride Cancelled Successfully", comment: ""), type: .success)
    }
    
    func rideCancelledByDriver() {
        self.showMessage(title: NSLocalizedString("Ride Cancelled", comment: ""), message: NSLocalizedString("Ride cancelled by the driver", comment: ""), type: .error)
    }
    
    func driverDidArrive() {
        statusLabel.text = NSLocalizedString("Your driver has been arrived", comment: "")
        showStatusView(with: CGRect(x: mapView.center.x - 125, y: confirmButton.frame.origin.y - 180, width: 250, height: 40))
    }
    
    func showError(error: String) {
        self.showMessage(title: NSLocalizedString("Error", comment: ""), message: error, type: .error)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func updateDriverLocation(driverLocation: CLLocationCoordinate2D, destinationLocation: CLLocationCoordinate2D) {
        self.mapView.drawPath(fromLocation: driverLocation, toLocation: destinationLocation) {
            self.drawDriverMarker()
            self.drawUserSelectedMarker()
            self.driverMarker.position = driverLocation
            self.userSelectedMarker.position = destinationLocation
        }
    }
    
    func resetView() {
        self.didPickupMarkerAddedOnCurrentLocation = false
        // this method is called when the user has to be taken to very initial stage.
        hideEstimateFareView()
        hideStatusView()
        hideDriverInfoView()
        resetLocationViews()
        mapView.clear()
        navigateToCurrentLocation()
        self.currentLocationButton.alpha = 1.0
        self.confirmButton.setTitle(NSLocalizedString("Confirm", comment: ""), for: .normal)
        self.confirmButton.backgroundColor = AppTheme.colorPrimary
        self.cancelButton.isHidden = true
    }
}

//MARK:- EstimateFare delegate
extension MapViewController: EstimateFareViewDelegate{
    func didTapDropDownButton(ofType: DropDownButtonType) {
        switch ofType {
        case .car:
            self.performSegue(withIdentifier: UIConstants.Segues.Ride.presentSelectionFromMap, sender: ["type": SelectionType.car, "data": viewModel.carsSelectionData()])
        case .driver:
            self.performSegue(withIdentifier: UIConstants.Segues.Ride.presentSelectionFromMap, sender: ["type": SelectionType.driver, "data": viewModel.driverSelectionData()])
        default:
            break
        }
    }
}

//MARK:- Selection VC delegate
extension MapViewController: SelectionDelegate{

    func didSelctItem(at indexPath: IndexPath, selectionType: SelectionType) {
        if selectionType == .car{
            viewModel.selectedRideId = (estimateFareView?.updateData(indexPath: indexPath)) ?? ""
            viewModel.estimatedValues = (estimateFareView?.getEstimatesForRide(id: viewModel.selectedRideId)) ?? [:]
        }else{
            estimateFareView?.setDriverName(name: viewModel.nameOfDriverAtIndex(index: indexPath.row))
            viewModel.selectedDriverId = viewModel.idForDriverAtIndex(index: indexPath.row)
        }
    }
    
    func dataForTableView(type: SelectionType) -> [Any] {
        if type == .car{
            return viewModel.carsSelectionData()
        }else{
            return viewModel.driverSelectionData()
        }
    }
}

extension MapViewController: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        var markerIndex = -1 // 0 for picup marker , 1 for drop marker
        if marker === pickupMarker{
            markerIndex = 0
            self.viewModel.setPickupCoordnates(coordinate: marker.position)
        } else if marker === dropMarker{
            markerIndex = 1
            self.viewModel.setDropCoordinates(coordinates: marker.position)
        }
        self.mapView.clear()
        self.drawMarkerIfAvailable()
        
        GMSGeocoder().reverseGeocodeCoordinate(marker.position) { (response, error) in
            if markerIndex == 0{
                self.pickupLocationTitleLabel.text = response?.results()?.first?.thoroughfare ?? NSLocalizedString("Pin Location", comment: "")
                self.pickupLocationDescLabel.text = response?.results()?.first?.lines?.first ?? "\(marker.position.latitude),\(marker.position.longitude)"
            }else if markerIndex == 1{
                self.dropLocationTitleLabel.text = response?.results()?.first?.thoroughfare ?? NSLocalizedString("Pin Location", comment: "")
                self.DropLocationDescLabel.text = response?.results()?.first?.lines?.first ?? "\(marker.position.latitude),\(marker.position.longitude)"
            }
        }
    }
}

//MARK:- Save place delegate
extension MapViewController: SavePlaceDelegate{
    func didTapSaveLocation(name: String, identifier: String) {
        if let popup = self.popup{
            popup.removePopup()
            self.showLoader()
            viewModel.saveLocation(location: identifier == viewModel.pickupLocationIdentifier ? viewModel.coordinatesForPickupLocation() : viewModel.dropCoordinates!, name: name, success: {
                self.hideLoading()
                self.showMessage(title: NSLocalizedString("Success", comment: ""), message: NSLocalizedString("Place Saved Successfully", comment: ""), type: .success)
            }, failure:  {
                self.hideLoading()
                self.showMessage(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Error in saving place", comment: ""), type: .error)
            })
        }
    }
    
    func didTapCancelButton() {
        if let popup = self.popup{
            popup.removePopup()
        }
    }
}
