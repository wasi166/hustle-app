//
//  SelectionViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 15/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

protocol SelectionDelegate: class{
    func didSelctItem(at indexPath: IndexPath, selectionType: SelectionType)
    func dataForTableView(type: SelectionType) -> [Any]
}

class SelectionViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    weak var delegate: SelectionDelegate?
    private let viewModel = SelectionViewModel()
    var selectionType = SelectionType.car { didSet { viewModel.selectionType = selectionType}}
    
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        viewModel.setDataArray(dataArray: delegate?.dataForTableView(type: selectionType) ?? [])
    }
    
    override func configureUI() {
        super.configureUI()
        
        self.view.backgroundColor = AppTheme.colorScreenBackground
        
        // tableview
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.layer.cornerRadius = 20.0
        tableView.clipsToBounds = true
        tableView.layer.masksToBounds = true
        tableView.layer.shadowRadius = 0.3
        tableView.layer.shadowColor = UIColor.black.cgColor
        tableView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        tableView.layer.shadowOpacity = 0.3
    }

    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- TableView delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = viewModel.dataForIndexPath(indexPath: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: data[BaseCellConstant.Identifier] as! String) as! BaseTableViewCell
        cell.configure(data: data, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelctItem(at: indexPath, selectionType: self.selectionType)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let data = viewModel.dataForSectionHeader(section: section)
        let headerView = tableView.dequeueReusableCell(withIdentifier: data[BaseCellConstant.Identifier] as! String) as! BaseTableViewCell
        headerView.configure(data: data, indexPath: IndexPath(row: 0, section: section))
        return headerView
    }
}
