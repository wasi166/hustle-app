//
//  InRideViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 03/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import EasyPopUp

class InRideViewController: BaseViewController, InRideDelegate {

    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var rideInfoView: UIView!
    
    @IBOutlet weak var remainingDistanceLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var completedTripsLabel: UILabel!
    @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var pickupAddressLabel: UILabel!
    @IBOutlet weak var dropLocationLabel: UILabel!
    @IBOutlet weak var dropLocationAddressLabel: UILabel!
    @IBOutlet weak var dropLocationPinImageView: UIImageView!
    @IBOutlet weak var editDropButton: UIButton!
    @IBOutlet weak var estimateFareButton: UIButton!
    @IBOutlet weak var shareLiveLocationButton: UIButton!
    @IBOutlet weak var reportAProblemButton: UIButton!
    @IBOutlet weak var estimateFareImageView: UIImageView!
    @IBOutlet weak var estimateTimeImageView: UIImageView!
    @IBOutlet weak var pickupLocationImageView: UIImageView!
    
    private var popup: EasyPopup?
    private let viewModel = InRideViewModel()
    var dataModel: NotificationDataModel?{
        didSet{
            if let model = self.dataModel{
                viewModel.setDataModel(model)
            }
        }
    }
    
    //MARK:- METHODS
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        viewModel.baseDelegate = self
    }
    
    override func configureUI() {
        super.configureUI()
        configureMapView()
        rideInfoView.backgroundColor = AppTheme.colorPrimary
        // remaining distance
        remainingDistanceLabel.textColor = AppTheme.colorText
        remainingDistanceLabel.font = AppTheme.fontText(ofSize: 10.0)
        distanceLabel.textColor = .white
        distanceLabel.font = AppTheme.fontText(ofSize: 14.0)
        // remining time label
        remainingTimeLabel.textColor = AppTheme.colorText
        remainingTimeLabel.font = AppTheme.fontText(ofSize: 10.0)
        timeLabel.textColor = .white
        timeLabel.font = AppTheme.fontText(ofSize: 14.0)
        // profile image view
        profileImageView.layer.cornerRadius = profileImageView.bounds.width/2
        profileImageView.clipsToBounds = true
        //name label
        nameLabel.textColor = .white
        nameLabel.font = AppTheme.fontText(ofSize: 14.0)
        // rating label
        ratingLabel.textColor = .white
        ratingLabel.font = AppTheme.fontText(ofSize: 14.0)
        // trips label
        completedTripsLabel.textColor = .white
        completedTripsLabel.font = AppTheme.fontText(ofSize: 12.0)
        // pickup location label
        pickupLocation.textColor = .white
        pickupLocation.font = AppTheme.fontText(ofSize: 12.0)
        pickupAddressLabel.textColor = .white
        pickupAddressLabel.font = AppTheme.fontText(ofSize: 10.0)
        // drop location
        dropLocationLabel.textColor = AppTheme.colorSecondary
        dropLocationLabel.font = AppTheme.fontText(ofSize: 12.0)
        dropLocationAddressLabel.textColor = AppTheme.colorSecondary
        dropLocationAddressLabel.font = AppTheme.fontText(ofSize: 10.0)
        dropLocationPinImageView.tintColor = AppTheme.colorSecondary
        // edit drop button
        editDropButton.setTitleColor(AppTheme.colorSecondary, for: .normal)
        editDropButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
        // estimate fare button
        estimateFareButton.titleLabel?.font = AppTheme.fontText(ofSize: 18.0)
        // share live location button
        shareLiveLocationButton.backgroundColor = AppTheme.colorSecondary
        shareLiveLocationButton.layer.cornerRadius = 10.0
        shareLiveLocationButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 14.0)
        // report a problem button
        reportAProblemButton.backgroundColor = AppTheme.colorRed
        reportAProblemButton.layer.cornerRadius = 10.0
        reportAProblemButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 14.0)
        
        // icon tint colors
        [estimateFareImageView, estimateTimeImageView, pickupLocationImageView]
            .forEach { (imageview) in
            // setting white in different way , because no idea why it is not working from .white color 🤷‍♂️
            imageview?.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
        }
        self.updateData()
    }

    private func configureMapView(){
        if let location = LocationManager.shared.lastReceivedLocation(){
            self.mapView.animate(toLocation: location)
            self.mapView.animate(toZoom: 10.0)
        }
        self.updateDriverLocation(driverLocation: dataModel?.ride?.driver?.driverLocation?.getCoordinates(), dropLocation: dataModel?.ride?.destination?.getCoordinates())
    }
    
    private func presentSaveDriverView(){
        let saveDriverView = Bundle.main.loadNibNamed(SaveDriverView.nibName, owner: self, options: [:])![0] as! SaveDriverView
        saveDriverView.delegate = self
        saveDriverView.driverName = viewModel.nameOfDriver()
        saveDriverView.layer.cornerRadius = 10
        popup = EasyPopup(superView: self.view, viewTopop: saveDriverView)
        popup?.showPopup()
    }
    
    //MARK:- IBActions
    
    @IBAction func didTapMarkDriverFavouriteButton(_ sender: Any) {
        presentSaveDriverView()
    }
    
    @IBAction func didTapEditDropButton(_ sender: Any) {
    }
    
    @IBAction func didTapShareLocationButton(_ sender: Any) {
    }
    
    @IBAction func didTapReportProblemButton(_ sender: Any) {
    }
    
    //MARK:- InRide Delegate
    func updateData() {
        distanceLabel.text = viewModel.remainingDistanceInRide()
        timeLabel.text = viewModel.reminingTimeInRide()
        nameLabel.text = viewModel.nameOfDriver()
        ratingLabel.text = viewModel.ratingOfDriver()
        completedTripsLabel.text = viewModel.completedTripsOfDriver()
        pickupLocation.text = viewModel.pickupLocationName()
        pickupAddressLabel.text = viewModel.pickupLocationAddress()
        dropLocationLabel.text = viewModel.dropLocationName()
        dropLocationAddressLabel.text = viewModel.dropLocationAddress()
        estimateFareButton.setTitle(viewModel.estimateFare(), for: .normal)
    }
    
    func rideEnded() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateDriverLocation(driverLocation: CLLocationCoordinate2D?, dropLocation: CLLocationCoordinate2D?) {
        if let driverLocation = driverLocation , let dropLocation = dropLocation{
            self.mapView.drawPath(fromLocation: driverLocation, toLocation: dropLocation, padding: 20.0) {
                let carMarker = GMSMarker(position: driverLocation)
                carMarker.icon = #imageLiteral(resourceName: "driver-marker")
                carMarker.map = self.mapView
                
                let dropMarker = GMSMarker(position: dropLocation)
                dropMarker.icon = #imageLiteral(resourceName: "drop-marker")
                dropMarker.map = self.mapView
            }
        }
    }
}

extension InRideViewController: SaveDriverDelegate{
    func didTapSaveDriver() {
        popup?.removePopup()
        self.showLoader()
        viewModel.saveDriver(success: {
            self.hideLoader()
            self.showMessage(title: "Success", message: "Successfully saved the driver", type: .success)
        }, failure: {
            self.hideLoader()
            self.showMessage(title: "Error", message: "Error in saving driver", type: .error)
        })
    }
    
    func didTapCancelButton() {
        popup?.removePopup()
    }
}
