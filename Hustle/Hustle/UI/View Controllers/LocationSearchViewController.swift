//
//  LocationSearchViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 20/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GooglePlaces

protocol LocationSearchControllerDelegate: class{
    func didCompleteWithPlace(place: SearchPlace)
}

class LocationSearchViewController: BaseViewController {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    private let searchController = UISearchController(searchResultsController: nil)
    private let viewModel = LocationSearchViewModel()
    
    weak var delegate: LocationSearchControllerDelegate?
    
    override func viewDidLoad() {
        self.baseModel = viewModel
        viewModel.baseDelegate = self
        super.viewDidLoad()
    }
    
    override func configureUI() {
        super.configureUI()
        self.title = "Select Location"
        // search controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search For Places"
        searchController.searchBar.delegate = self
        searchController.delegate = self
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            
        }
        definesPresentationContext = true
        // segment control
        segmentControl.tintColor = AppTheme.colorPrimary
        // Cancel Item
        let cancelItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(didTapCancelItem))
        self.navigationItem.leftBarButtonItem = cancelItem
    }
    
    @objc func didTapCancelItem(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didChangeSegmentControlValue(_ sender: UISegmentedControl) {
        viewModel.didChangeSegmentValue(value: sender.selectedSegmentIndex)
    }
}

//MARK:- View Model Delegate
extension LocationSearchViewController: locationSearchDelegate{
    func didUpdateLocationData() {
        self.tableView.reloadData()
    }
}

//MARK:- UITableview Delegate
extension LocationSearchViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LocationSearchTableViewCell.identifier) as! LocationSearchTableViewCell
        cell.configure(data: viewModel.dataForIndexPath(indexPath: indexPath), indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didCompleteWithPlace(place: viewModel.dataForIndexPath(indexPath: indexPath))
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK:- Search Result updating
extension LocationSearchViewController: UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate{
    
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.becomeFirstResponder()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.didChangeSearchText(text: searchController.searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: true, completion: nil)
    }
}
