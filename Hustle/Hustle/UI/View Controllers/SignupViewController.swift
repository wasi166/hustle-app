//
//  SignupViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 29/06/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class SignupViewController: BaseViewController, SignupViewDelegate {

    //FIXME: Change the name of text fields
    @IBOutlet weak var signupLabel: UILabel!
    @IBOutlet weak var nameField: UITextField! // first name
    @IBOutlet weak var emailField: UITextField! // last name
    @IBOutlet weak var passwordField: UITextField! // email
    @IBOutlet weak var confirmPasswordField: UITextField! // phone number
    @IBOutlet weak var signupButton: UIButton!

    var token = ""
    private let viewModel = SignupViewModel()
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        viewModel.baseDelegate = self
    }
    
    override func configureUI() {
        super.configureUI()
        self.view.backgroundColor = AppTheme.colorScreenBackground
        // signup label
        self.signupLabel.textColor = .black
        self.signupLabel.font = AppTheme.fontHeading(ofSize: 24.0)
        //text fields
        [nameField, emailField, passwordField, confirmPasswordField]
            .forEach { (field) in
                field!.backgroundColor = .white
                field!.layer.cornerRadius = 10.0
                field!.font = AppTheme.fontText(ofSize: 14.0)
                field!.makeThemedTextField()
                field!.configureLeftView()
        }
        // sign up button
        self.signupButton.backgroundColor = AppTheme.colorPrimary
        self.signupButton.setTitleColor(.white, for: .normal)
        self.signupButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 18.0)
        self.signupButton.layer.cornerRadius = 10.0
        
    }

    @IBAction func didTapSignupButton(_ sender: Any) {
        if viewModel.isFormValid(name: nameField.text!, email: emailField.text!, password: passwordField.text!, confirmPassword: confirmPasswordField.text!){
            viewModel.signUp(firstName: nameField.text!, lastName: emailField.text!, email: passwordField.text!, phone: confirmPasswordField.text!, otp: token)
        }else{
            self.showMessage(title: NSLocalizedString("Error", comment: ""), message: viewModel.errorForForm(name: nameField.text!, email: emailField.text!, password: passwordField.text!, confirmPassword: confirmPasswordField.text!), type: .error)
        }
    }
    
    //MARK:- Signup delegate
    func didSignupSuccessfully() {
        let mapViewController = UIStoryboard(name: UIConstants.Storyboards.ride, bundle: Bundle.main).instantiateViewController(withIdentifier: UIConstants.ViewControllerIdentifiers.Ride.mapVC)
        UIApplication.shared.delegate?.window??.rootViewController = mapViewController
    }
    
    func didFailToSignup(error: String) {
        self.showMessage(title: NSLocalizedString("Error", comment: ""), message: error, type: .error)
    }
}
