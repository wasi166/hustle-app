//
//  ProfileViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 08/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelBarButtonItem: UIButton!
    @IBOutlet var titleLabels: [UILabel]!
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet var editButtons: [UIButton]!
    @IBOutlet weak var saveButton: UIButton!
    
    private let viewModel = ProfileViewModel()
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        self.setUserDataInFields()
    }
    
    override func configureUI() {
        super.configureUI()
        // header view
        headerView.backgroundColor = AppTheme.colorPrimary
        titleLabel.textColor = .white
        titleLabel.font = AppTheme.fontHeading(ofSize: 20.0)
        cancelBarButtonItem.titleLabel?.font = AppTheme.fontText(ofSize: 16.0)
        cancelBarButtonItem.setTitleColor(.white, for: .normal)
        // title labels
        for label in titleLabels{
            label.textColor = AppTheme.colorText
            label.font = AppTheme.fontText(ofSize: 12.0)
        }
        // edit buttons
        for button in editButtons{
            button.setTitleColor(AppTheme.colorSecondary, for: .normal)
            button.setTitleColor(AppTheme.colorSecondary, for: .selected)
            button.adjustsImageWhenDisabled = false
            button.titleLabel?.font = AppTheme.fontHeading(ofSize: 14.0)
            button.setTitle(NSLocalizedString("Edit", comment: ""), for: .normal)
            button.setTitle("Done", for: .selected)
            button.isHighlighted = false
        }
        // text fields
        for field in textFields{
            field.font = AppTheme.fontText(ofSize: 14.0)
            field.textColor = AppTheme.colorText
            field.isUserInteractionEnabled = false
        }
        // save button
        saveButton.backgroundColor = AppTheme.colorPrimary
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
        saveButton.layer.cornerRadius = 10.0
    }
    
    private func setUserDataInFields(){
        for i in 0..<self.textFields.count{
            self.textFields[i].text = viewModel.getUserData()[i]
        }
    }
    
    //MARK:- IBActions
    @IBAction func didTapEditButtons(_ sender: UIButton){
        // if sender button is not selected then open keyboard for that field.
        // if it is selected then keyboard is already shown.
        if !sender.isSelected{
            // unselect all the buttons to remove already selected button if there is any.
            for i in 0...3{
                editButtons[i].isSelected = false
                textFields[i].isUserInteractionEnabled = false
            }
            self.textFields[sender.tag].isUserInteractionEnabled = true
            self.textFields[sender.tag].becomeFirstResponder()
        }else{
            self.textFields[sender.tag].resignFirstResponder()
            self.textFields[sender.tag].isUserInteractionEnabled = false
        }
        sender.isSelected = !sender.isSelected // toggle the selected property of sender button
    }

    @IBAction func didTapSaveButton(_ sender: Any) {
        viewModel.saveProfile(userData: self.textFields.map({ (field) -> String in
            return field.text!
        }), success: {
            self.showMessage(title: NSLocalizedString("Success", comment: ""), message: "Profile Saved Successfully", type: .success)
            self.dismiss(animated: true, completion: nil)
        }) { (message) in
            self.showMessage(title: NSLocalizedString("Error", comment: ""), message: message, type: .error)
        }
    }
    
    @IBAction func didTapCancelButton(){
        self.dismiss(animated: true, completion: nil)
    }
}
