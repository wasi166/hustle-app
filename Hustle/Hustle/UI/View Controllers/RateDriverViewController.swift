//
//  RateDriverViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 28/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import Cosmos

class RateDriverViewController: BaseViewController {

    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var arrivedLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var tripsLabel: UILabel!
    @IBOutlet weak var cosmosTitleLabel: UILabel!
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var sendRateButton: UIButton!
    @IBOutlet weak var fareLabel: UILabel!
    @IBOutlet weak var reportProblemButton: UIButton!
    
    private let viewModel = RateDriverViewModel()
    var dataModel: NotificationDataModel?{
        didSet{
            if let model = self.dataModel{
                viewModel.dataModel = model
            }
        }
    }
    
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        if let _ = self.dataModel{
            self.populateData()
        }
    }
    
    override func configureUI() {
        super.configureUI()
        self.view.backgroundColor = AppTheme.colorPrimary
        // arrived label
        arrivedLabel.textColor = .white
        arrivedLabel.font = AppTheme.fontText(ofSize: 18.0)
        // fare label
        fareLabel.textColor = .white
        fareLabel.font = AppTheme.fontHeading(ofSize: 40.0)
        // profile image view
        profileImageView.layer.cornerRadius = profileImageView.bounds.width/2
        // name label
        nameLabel.textColor = .white
        nameLabel.font = AppTheme.fontText(ofSize: 12.0)
        // rating label
        ratingLabel.textColor = .white
        ratingLabel.font = AppTheme.fontText(ofSize: 12.0)
        // trips label
        tripsLabel.textColor = .white
        tripsLabel.font = AppTheme.fontText(ofSize: 10.0)
        // cosmos title label
        cosmosTitleLabel.textColor = AppTheme.colorText
        cosmosTitleLabel.font = AppTheme.fontText(ofSize: 14.0)
        // cosmos view
        cosmosView.settings.fillMode = .full
        cosmosView.settings.emptyBorderColor = .white
        // send rate button
        sendRateButton.layer.cornerRadius = 10.0
        sendRateButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 14.0)
        sendRateButton.setTitleColor(.white, for: .normal)
        sendRateButton.backgroundColor = AppTheme.colorSecondary
        // report problem button
        reportProblemButton.layer.cornerRadius = 10.0
        reportProblemButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 14.0)
        reportProblemButton.setTitleColor(.white, for: .normal)
        reportProblemButton.backgroundColor = AppTheme.colorRed
        // skip button
        skipButton.setTitleColor(.white, for: .normal)
        skipButton.titleLabel?.font = AppTheme.fontText(ofSize: 15.0)
    }
    
    private func populateData(){
        self.fareLabel.text = viewModel.fareForRide()
        self.nameLabel.text = viewModel.getDriverFullName()
        self.ratingLabel.text = viewModel.getDriverRating()
        self.tripsLabel.text = viewModel.getDriverTotalTrips()
    }
    
    //MARK:- IBActions
    
    @IBAction func didTapSkupButton(_ sender: Any) {
        SessionManager.shared.removeCurrentTrip()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapSendRateButton(_ sender: Any) {
        if self.cosmosView.rating == 0.0{
            self.showMessage(title: NSLocalizedString("Error", comment: ""), message: viewModel.errorForInvalidRating(), type: .error)
        }else{
            viewModel.sendRate(rating: Int(self.cosmosView!.rating)) { (success) in
                if success{
                    SessionManager.shared.removeCurrentTrip()
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.showMessage(title: NSLocalizedString("Error", comment: ""), message: self.viewModel.errorForFailedSendRating(), type: .error)
                }
            }
        }
    }
    
    @IBAction func didTapReportProblemButton(_ sender: Any) {
    }
}
