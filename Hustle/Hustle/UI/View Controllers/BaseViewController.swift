//
//  ViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 28/06/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import SwiftMessageBar
import NVActivityIndicatorView

class BaseViewController: UIViewController {

    var baseModel = BaseViewModel()
    private var loaderView: NVActivityIndicatorView?
    private let loaderContainerView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseModel.viewDidLoad()
        self.configureUI()
        self.configureMessageBar()
        if loaderView == nil{
            self.configureLoaderView()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        baseModel.viewWillAppear()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        baseModel.viewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        baseModel.viewWillDissappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        baseModel.viewDidDissappear()
    }
    
    func configureUI(){
    }
    
    // shows the message on the screen
    func showMessage(title: String ,message: String, type: MessageType){
        SwiftMessageBar.showMessage(withTitle: title, message: message, type: type)
    }
    
    // shows the animated loader on the screen.
    func showLoader(){
        if let loaderView = self.loaderView{
            loaderView.startAnimating()
            self.view.addSubview(loaderContainerView)
        }
    }
    
    // hides the animating loader from the screen.
    func hideLoader(){
        if let loaderView = self.loaderView{
            if loaderView.isAnimating{
                loaderView.stopAnimating()
                loaderContainerView.removeFromSuperview()
            }
        }
    }
    
    // configures the custom message bar for app.
    private func configureMessageBar(){
        let messageConfig = SwiftMessageBar.Config.Builder()
            .withSuccessColor(AppTheme.colorSuccess)
            .withErrorColor(AppTheme.colorError)
            .withTitleFont(AppTheme.fontHeading(ofSize: 18.0))
            .withMessageFont(AppTheme.fontHeading(ofSize: 15.0))
            .build()
        SwiftMessageBar.setSharedConfig(messageConfig)
    }
    
    
    private func configureLoaderView(){
        loaderContainerView.frame = CGRect(x: 0, y: 40, width: self.view.bounds.width, height: self.view.bounds.height - 180)
        loaderView = NVActivityIndicatorView(frame: CGRect(x: loaderContainerView.center.x - 40, y: loaderContainerView.center.y - 40, width: 80, height: 80), type: .ballScaleRippleMultiple, color: AppTheme.colorPrimary, padding: 1.0)
        loaderContainerView.addSubview(loaderView!)
    }
}

