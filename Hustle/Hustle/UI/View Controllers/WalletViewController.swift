//
//  WalletViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 07/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class WalletViewController: BaseViewController {

    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var doneItemButton: UIButton!
    @IBOutlet weak var walletCreditTitleLabel: UILabel!
    @IBOutlet weak var walletAmountLabel: UILabel!
    
    private let viewModel = WalletViewModel()
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        viewModel.getWalletAmount { (amount) in
            self.walletAmountLabel.text = amount
        }
    }
    
    override func configureUI() {
        super.configureUI()
        
        navigationView.backgroundColor = AppTheme.colorPrimary
        titleLabel.textColor = .white
        titleLabel.font = AppTheme.fontHeading(ofSize: 16.0)
        doneItemButton.setTitleColor(.white, for: .normal)
        doneItemButton.titleLabel?.font = AppTheme.fontText(ofSize: 16.0)
        // wallet credit title label
        walletCreditTitleLabel.textColor = AppTheme.colorText
        walletCreditTitleLabel.font = AppTheme.fontText(ofSize: 16.0)
        // wallet amount label
        walletAmountLabel.textColor = AppTheme.colorText
        walletAmountLabel.font = AppTheme.fontText(ofSize: 18.0)
    }
    
    @IBAction func didTapDoneItemButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
