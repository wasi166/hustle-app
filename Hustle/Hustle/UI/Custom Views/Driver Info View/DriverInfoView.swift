//
//  DriverInfoView.swift
//  Hustle
//
//  Created by  Wasi Tariq on 28/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import MessageUI

enum CarColorType: String{
    case red = "Red"
    case black = "Black"
    
    func color() -> UIColor{
        switch self {
        case .red:
            return .red
        case .black:
            return .black
        }
    }
}

class DriverInfoView: UIView {

    static let nibName = "DriverInfo"
    static let height = 130.0
    static let width = 320.0
    
    private var dataModel: RideDriverModel?
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var tripsLabel: UILabel!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carColorLabel: UILabel!
    @IBOutlet weak var carNumberLabel: UILabel!
    
    override func draw(_ rect: CGRect) {
        configureUI()
    }
    
    private func configureUI(){
        // profile imageview
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = profileImageView.bounds.width/2
        
        //name label
        nameLabel.textColor = AppTheme.colorSecondary
        nameLabel.font = AppTheme.fontText(ofSize: 16.0)
        
        // rating label
        ratingLabel.textColor = AppTheme.colorText
        ratingLabel.font = AppTheme.fontText(ofSize: 14.0)
        
        // trips label
        tripsLabel.textColor = AppTheme.colorText
        tripsLabel.font = AppTheme.fontText(ofSize: 12.0)
        
        // car name label
        carNameLabel.textColor = AppTheme.colorText
        carNameLabel.font = AppTheme.fontText(ofSize: 12.0)
        
        // car color label
        carColorLabel.font = AppTheme.fontHeading(ofSize: 14.0)
        
        // car number label
        carNumberLabel.textColor = AppTheme.colorSecondary
        carNumberLabel.font = AppTheme.fontHeading(ofSize: 16.0)
    }
    
    func populateData(with model: RideDriverModel?){
        if let model = model{
            self.dataModel = model
            nameLabel.text = model.getFullName()
            ratingLabel.text = model.getRatingText()
            tripsLabel.text = model.getCompletedTripsText()
            carNameLabel.text = model.driverVehicle?.VehicleName
            carColorLabel.text = model.driverVehicle?.VehicleColor
            carNumberLabel.text = model.driverVehicle?.ModelNumber
            if let carColor = CarColorType(rawValue: (model.driverVehicle?.VehicleColor) ?? ""){
                carColorLabel.textColor = carColor.color()
                carImageView.tintColor = carColor.color()
            }
        }
    }

    
    //MARK:- IBActions
    @IBAction private func callDriverTapped(){
        if let model = self.dataModel , let phone = model.PhoneNumber{
            UIApplication.shared.open(URL(string: "tel://"+phone)!, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction private func messageDriverTapped(){
        if let model = self.dataModel, let phone = model.PhoneNumber{
            let messageController = MFMessageComposeViewController()
            messageController.recipients = [phone]
            messageController.body = ""
            messageController.messageComposeDelegate = self
            //TODO: think how to present this message view controller from uiview.
        }
        
    }
}

extension DriverInfoView: MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
    }
}
