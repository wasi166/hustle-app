//
//  SavePlaceView.swift
//  Hustle
//
//  Created by  Wasi Tariq on 19/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

protocol SavePlaceDelegate: class {
    func didTapSaveLocation(name: String, identifier: String)
    func didTapCancelButton()
}

class SavePlaceView: BaseView {

    static let nibName = "SavePlace"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var canceButton: UIButton!
    
    var identifier = ""
    weak var delegate: SavePlaceDelegate?
    
    override func configureUI() {
        super.configureUI()
        
        // title label
        titleLabel.text = NSLocalizedString("Save Place", comment: "")
        titleLabel.textColor = .white
        titleLabel.font = AppTheme.fontHeading(ofSize: 18.0)
        titleLabel.backgroundColor = AppTheme.colorPrimary
        // name field
        nameField.textColor = AppTheme.colorText
        nameField.layer.borderColor = AppTheme.colorText.cgColor
        nameField.layer.borderWidth = 0.5
        nameField.layer.cornerRadius = 10.0
        nameField.font = AppTheme.fontText(ofSize: 14.0)
        // error label
        errorLabel.textColor = AppTheme.colorRed
        errorLabel.font = AppTheme.fontText(ofSize: 10.0)
        errorLabel.text = NSLocalizedString("Name cannot be left empty", comment: "")
        errorLabel.isHidden = true
        // save button
        saveButton.setTitle(NSLocalizedString("Save", comment: ""), for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 14.0)
        saveButton.backgroundColor = AppTheme.colorGreen
        saveButton.layer.cornerRadius = 10.0
        // cancel button
        canceButton.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
        canceButton.setTitleColor(AppTheme.colorRed, for: .normal)
        canceButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 14.0)
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        if let text = nameField.text, !text.isEmpty{
            errorLabel.isHidden = true
            delegate?.didTapSaveLocation(name: text, identifier: self.identifier)
        }else{
            errorLabel.isHidden = false
        }
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        delegate?.didTapCancelButton()
    }
}
