//
//  EstimateFareView.swift
//  Hustle
//
//  Created by  Wasi Tariq on 11/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

enum DropDownButtonType {
    case car
    case driver
    case payment
}

protocol EstimateFareViewDelegate: class{
    func didTapDropDownButton(ofType: DropDownButtonType)
}

class EstimateFareView: UIView {

    static let nibName = "EstimateFare"
    static let width = 320.0
    static let height = 150.0
    
    var dataModel: EstimateFareDataModel? {didSet {let _ = self.updateData(indexPath: IndexPath.init(row: 0, section: 0))}}
    
    @IBOutlet weak var kmButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var fareButton: UIButton!
    @IBOutlet weak var carDropDownButton: UIButton!
    @IBOutlet weak var driverDropDownButton: UIButton!
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var cardNumberDropDownButton: UIButton!
    
    weak var delegate: EstimateFareViewDelegate?
    
    override func draw(_ rect: CGRect) {
        configureUI()
    }
    
    private func configureUI(){
        [kmButton, timeButton].forEach { (button) in
            button?.titleLabel?.font = AppTheme.fontText(ofSize: 12.0)
        }
        
        [carDropDownButton, driverDropDownButton, cardNumberDropDownButton].forEach { (button) in
            button?.setTitleColor(AppTheme.colorSecondary, for: .normal)
            button?.titleLabel?.font = AppTheme.fontText(ofSize: 12.0)
            button?.contentHorizontalAlignment = .right
        }
        
        cardLabel.textColor = AppTheme.colorSecondary
        cardLabel.font = AppTheme.fontText(ofSize: 12.0)
        fareButton.titleLabel?.font = AppTheme.fontText(ofSize: 10.0)
    }
    
    func updateData(indexPath: IndexPath) -> String{
        if let model = dataModel{
            self.carDropDownButton.setTitle(model.carName(forIndexPath: indexPath), for: .normal)
            self.timeButton.setTitle(model.duration(), for: .normal)
            self.fareButton.setTitle(model.estimateFare(forIndexPath: indexPath), for: .normal)
            self.kmButton.setTitle(model.distance(), for: .normal)
            return model.rideID(forIndexPath: indexPath)
        }
        return ""
    }
    
    func getEstimatesForRide(id: String) -> Dictionary<String, Any>{
        var estimatedValues = Dictionary<String, Any>()
        if let model = dataModel{
            estimatedValues["EstimatedDistance"] = model.element?.Distance?.Value
            estimatedValues["EstimatedTime"] = model.element?.Duration?.Value
            for i in model.lstRideType!{
                for j in i.lstCategory!{
                    if j.Id! == id{
                        estimatedValues["EstimatedMaxCost"] = j.MaxCost ?? 0.0
                        estimatedValues["EstimatedMinCost"] = j.MinCost ?? 0.0
                        break
                    }
                }
            }
        }
        return estimatedValues
    }
    
    func setDriverName(name: String){
        driverDropDownButton.setTitle(name, for: .normal)
    }
    
    //MARK:- IBActions
    @IBAction func didTapCarButton(_ sender: Any) {
        self.delegate?.didTapDropDownButton(ofType: .car)
    }
    
    @IBAction func didTapDriverButton(_ sender: Any) {
        self.delegate?.didTapDropDownButton(ofType: .driver)
    }
    
    @IBAction func didTapCardButton(_ sender: Any) {
        self.delegate?.didTapDropDownButton(ofType: .payment)
    }
}
