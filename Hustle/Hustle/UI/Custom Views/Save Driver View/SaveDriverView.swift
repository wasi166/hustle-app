//
//  SaveDriverView.swift
//  Hustle
//
//  Created by  Wasi Tariq on 26/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

protocol SaveDriverDelegate: class{
    func didTapSaveDriver()
    func didTapCancelButton()
}

class SaveDriverView: BaseView {
    
    static let nibName = "SaveDriver"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    weak var delegate: SaveDriverDelegate?
    var driverName = ""{
        didSet{
            self.nameLabel.text = self.driverName
        }
    }
    
    override func configureUI() {
        super.configureUI()
        // title label
        titleLabel.textColor = .black
        titleLabel.font = AppTheme.fontHeading(ofSize: 16.0)
        // profile image view
        profileImageView.layer.cornerRadius = profileImageView.bounds.height/2
        profileImageView.clipsToBounds = true
        profileImageView.layer.borderColor = AppTheme.colorPrimary.cgColor
        profileImageView.layer.borderWidth = 0.5
        // name label
        nameLabel.textColor = .black
        nameLabel.font = AppTheme.fontText(ofSize: 16.0)
        nameLabel.text = self.driverName
        // save button
        saveButton.backgroundColor = AppTheme.colorPrimary
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
        saveButton.layer.cornerRadius = 4.0
        // cancel button
        cancelButton.setTitleColor(AppTheme.colorRed, for: .normal)
        cancelButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        delegate?.didTapSaveDriver()
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        delegate?.didTapCancelButton()
    }
    
}
