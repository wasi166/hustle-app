//
//  AppDelegate.swift
//  Hustle
//
//  Created by  Wasi Tariq on 28/06/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//
// ea4b3fc28c240600184ae8a1759778cdd806aa3b
import UIKit
import CoreData
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications
import Crashlytics
import Fabric

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let viewModel = ApplicationViewModel()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
       
        Fabric.with([Crashlytics.self])
        GMSServices.provideAPIKey("AIzaSyDZV57Xyc4z_Nh6hi9uS52GrTQvmPFq2HU")
        GMSPlacesClient.provideAPIKey("AIzaSyDZV57Xyc4z_Nh6hi9uS52GrTQvmPFq2HU")

        // setting initial view controller according to the logged in state
        let storyboard = UIStoryboard(name: SessionManager.shared.isLoggedin() ? UIConstants.Storyboards.ride : UIConstants.Storyboards.registration, bundle: Bundle.main)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: SessionManager.shared.isLoggedin() ? UIConstants.ViewControllerIdentifiers.Ride.mapVC : UIConstants.ViewControllerIdentifiers.Registrations.startNavigationController)
        window?.rootViewController = initialViewController
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        UNUserNotificationCenter.current().delegate = self
        application.registerForRemoteNotifications()

        // starting location manager
        LocationManager.shared.startUpdatingLocation()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Hustle")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("App will present a new notification")
        print(notification.request.content.userInfo)
        NotificationManager.shared.didRecieveNotification(data: notification.request.content.userInfo as! Dictionary<String, Any>)
        completionHandler([.badge, .sound, .alert])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("App did recieve remote notification")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Device Token", deviceTokenString)    }
}

extension AppDelegate: MessagingDelegate{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        debugPrint("FCM Token: \(fcmToken)")
        SessionManager.shared.registerFCMToken(fcmToken)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        debugPrint(remoteMessage.appData)
        NotificationManager.shared.didRecieveNotification(data: remoteMessage.appData as! Dictionary<String, Any>)
    }
    
    
    func temp(){
        let _ = [AnyHashable("data"): [
            "DistADurBwDvrADest": "null",
            
            "DistADurBwCustADvr": [
                "Status" : "Success!",
                "Duration":["Value":471,"Text":"8 mins"],
                "Distance":["Value": 2753,"Text":"1.7 mi"]
            ],
            "notificationType":1,

            "ride":["markArrived":"null",
                    "rideTimings":["Active":"null","Id":"null","RideStartTime":"null","RideEndTime":"null"],
                "driver":["FireBaseToken":"epvlxpo0dgM:APA91bGiQEl3Obs_lwafdDAzwDCRuwbMI3HxWbJ-0udHJVFKlot0nX7HKy9_3QQM_I5cVPgC4b78uSPopPXLz-2YgDOT-7VHTP9EbyRBcbALEkr_sO0FFAX-ZTF-7yQ4Yh_bX8aDGY8M","driverLocation":["Latitude":24.897276,"Longitude":67.077624,"Name":"null"],
                    "driverWallet":["Active":false,"Amount":-59.0,"Id":"a372a475-b8f6-4eb6-84dd-b344891435f6"],
                    "VerifyPhone":false,"FirstName":"Abdul Moeed","Image":"null","EmailAddress":"null","DateTime":"0001-01-01T08:00:00Z","CompletedRides":5,"VerifyCode":false,
                    "ratings":["AverageRating":4.0,"Id":"11048f84-ff57-4b17-8e08-afe67654484c"],
                    "ActiveUser":false,
                    "driverVehicle":["ModelNumber":"ABA-123",
                    "vehicleCompany":["CompanyName":"Suzuki","VehicleLogo":"null","Id":"1D1F6198-7415-49CA-B12A-9E1CDECDD8E8","ActiveCompany":false],
                    "Active":false,
                    "ChasisNumber":"12345",
                    "VehicleColor":"Silver",
                    "VehicleName":"Alto VXR",
                    "VehicleYear":"2012",
                    "Id":"7D402D00-1EEF-4A1D-8321-08B64E0784A7","DateTime":"null","vehicleType":["Active":false,"Number":0,"Title":"4 wheller","Id":"3AD414F6-99AB-4301-B14F-6F8AFAE35108"],"VehicleOwnerName":"Muhammad Usama Khan"]
                    ,"PhoneNumber":"+923442928117","Id":"500D6989-CFB8-4F50-942D-911119C015C1","LastName":"Khan","Password":"null","status":"null"],
                    "rideTypeCategory":["PerKilometerCost":17.11,"rideType":["Title":"Rides","Id":"31376EBC-EB55-47BA-AF26-529CF06B5115"],
                        "TotalWaitingTime":1.5, "BaseFare":95.51, "Title":"Taxi","Tax":2.5, "Id":"29657048-8C9C-4D2F-83D6-F2051F2F8697","PerTimeCost":16.14,"Logo":"null"],
                    
                    "destination":["Latitude":24.8935997,"Longitude":67.0713131,"Name":"Agha Khan Hospital Pond No.1"],
                    "rideStatus":["Number":2,"Title":"Accepted","Id":"E18BC589-9BEF-47EC-ADFA-7565DCC993D8"],
                    "estimatedFare": ["EstimatedTime":699.0,"EstimatedMinCost":262.32, "EstimatedMaxCost":275.44,"Active":true, "Id":"00442316-dee9-4d68-b481-e8250caca882","DateTime":"7272019 7:32:44 AM", "EstimatedDistance":3989.0]
                    ,"Id":"924bf82a-43a9-43da-b83b-963b6a5a631f",
                     "source":["Latitude":24.89722279999999,"Longitude":67.077536,"Name":"Metro 1 News"],
                     "DateTime":"2019-07-27T14:32:44.82Z",
                     "customer":["FireBaseToken": "f5DBZTzjpY0:APA91bEJ_CMRotFaxQ-kfE53ToD5weoOQtd1T10FyEEwXZlHTlUdd5b7LmSSgjMuLB_1oDzCM48c_-c8oaqyj3XKsz6NS_tF474KoAnA8Nc3ozOf-hQTOp2J60qln0pC0X_cDiS90mbX", "FirstName":"vhjj",
                                 "ratings":["AverageRating":0.0, "Id":"null"],
                                 "PhoneNumber":"+923243305445",
                                 "Id":"9c110629-a7c4-4e40-ba5e-98d950678157",
                                 "LastName":"chhj",
                                 "customerWallet":["Active":false,"Amount":0.0,"Id":"null"],
                                 "Image":"null","EmailAddress":"null","status":"null"],
                     "rideFare":["TotalFare":0.0,"datetime":"null","TotalFareTime":"0","currency":["Title":"null","Id":"null","Code":"null"],"TotalFarePaid":0.0,"Id":"null","TotalWaitingTimeCost":0.0,"TotalDistance":0.0,"RideTax":"null"]]
            
            ,"status":"Successfully retireved the info"], AnyHashable("collapse_key"): "com.hustle.iosApp.Hustle", AnyHashable("from"): 69] as [AnyHashable : Any]
    }
}
