//
//  ServiceConstants.swift
//  Hustle
//
//  Created by  Wasi Tariq on 03/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

class ServiceConstants{

    private static let devBaseURL = "https://hustledevapp.com"
    private static let prodBaseURL = "https://hustleprodapp.com"
    // version URLs
    private static let versionOneURL = "/api/v1"
    private static let versonTwoURL = "/api/v2"
    
    private static func urlWithVersionOne(url: String) -> String{
        return ServiceConstants.baseURL+versionOneURL+url
    }
    
    private static func urlWithVersionTwo(url: String) -> String{
        return ServiceConstants.baseURL+versonTwoURL+url
    }
    
    // base URL
    private static let baseURL = ServiceConstants.prodBaseURL
    
    // validate number
    static let validateNumberEndpoint = urlWithVersionOne(url: "/ValidateNumber")
    
    // Verify number
    static let verifyNumberEndpoint = urlWithVersionOne(url: "/VerifyCustomerNumber")
    
    // Registration
    static let registerEndpoint = urlWithVersionOne(url: "/RegisterCustomer")
    
    // Estimate fare
    static let estimateFareEndpoint = urlWithVersionOne(url: "/EstimateFare")
 
    // Request Ride
    static let requestRideEndpoint = urlWithVersionOne(url: "/RequestForRide")
    
    // Get Ride Details
    static let rideDetailsEndPoint = urlWithVersionOne(url: "/GetRideDetails")
    
    // Cancel Current Ride
    static let cancelCurrentRideEndpoint = urlWithVersionOne(url: "/CancelRideByCustomer")
    
    // rate driver
    static let rateDriverEndpoint = urlWithVersionOne(url: "/SaveCustomerFeedback")
    
    // get wallet amount
    static let walletAmountEndoint = urlWithVersionOne(url: "/GetCustomerWalletamount")
    
    // save user profile
    static let saveProfileEndpoint = urlWithVersionOne(url: "/UpdateCustomerProfile")
    
    // user pending rides
    static let pendingRidesEndpoint = urlWithVersionOne(url: "/CheckCustomerPendingRide")
    
    // User Ride History
    static let rideHistoryEndpoint = urlWithVersionOne(url: "/GetCustomerRidesHistory")
    
    // save favourite place
    static let savePlaceEndpoint = urlWithVersionTwo(url: "/SaveFavouritePlace")
    
    // get saved places
    static let getSavedPlacesEndpoint = urlWithVersionTwo(url: "/GetAllFavouritePlaces")
    
    // save driver
    static let saveDriverEndpoint = urlWithVersionTwo(url: "/SaveFavouriteDriver")
    
    // get saved driver
    static let getSavedDriversEndpoint = urlWithVersionTwo(url: "/GetAllFavouriteDrivers?CustomerId=")
}
