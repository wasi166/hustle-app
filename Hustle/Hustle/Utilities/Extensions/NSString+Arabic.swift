//
//  NSString+Arabic.swift
//  Hustle
//
//  Created by  Wasi Tariq on 31/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

extension String{
    func getEnglishNumbers() -> String{
        let formatter: NumberFormatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
        if let final = formatter.number(from: self){
            return String(describing: final)
        }
        return ""
    }
}
