//
//  GMSMapView.swift
//  Hustle
//
//  Created by  Wasi Tariq on 02/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation
import GoogleMaps
import Alamofire

private struct MapPath : Decodable{
    var routes : [Route]?
}

private struct Route : Decodable{
    var overview_polyline : OverView?
}

private struct OverView : Decodable {
    var points : String?
}

extension MapViewController {
    
    //MARK:- Call API for polygon points
    
    func drawPolygon(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let origin = "\(source.latitude),\(source.longitude)"
        let destination = "\(destination.latitude),\(destination.longitude)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyDZV57Xyc4z_Nh6hi9uS52GrTQvmPFq2HU"
        
        //FIXME: What alamofire doing here when we have Service manager?
        Alamofire.request(url).responseJSON { response in
            do{
                let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String: Any]
                let routes = json["routes"] as! [[String: Any]]
                
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"] as! [String: Any]
                    let points = routeOverviewPolyline["points"] as! String
                    self.drawPath(with: points)
                }
            } catch{}
        }
    }
    
    // this functions is used for drawing navigation path
    func drawNaivagtionPath(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D){
        let origin = "\(from.latitude),\(from.longitude)"
        let destination = "\(to.latitude),\(to.longitude)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyDZV57Xyc4z_Nh6hi9uS52GrTQvmPFq2HU"
        
        //FIXME: What alamofire doing here when we have Service manager?
        Alamofire.request(url).responseJSON { response in
            do{
                let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String: Any]
                let routes = json["routes"] as! [[String: Any]]
                
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"] as! [String: Any]
                    let points = routeOverviewPolyline["points"] as! String
                    self.drawPath(with: points)
                }
            } catch{}
        }
    }
    
    //MARK:- Draw polygon
    
    private func drawPath(with points : String){
        
        DispatchQueue.main.async {
            
            let path = GMSPath(fromEncodedPath: points)
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 3.0
            polyline.strokeColor = AppTheme.colorSecondary
            polyline.map = self.mapView
            
            let bounds = GMSCoordinateBounds(path: path!)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100.0))
        }
    }
}

extension GMSMapView{
    
    func drawPath(fromLocation: CLLocationCoordinate2D, toLocation: CLLocationCoordinate2D, padding: CGFloat = 100.0, success: @escaping() -> Void){
        let origin = "\(fromLocation.latitude),\(fromLocation.longitude)"
        let destination = "\(toLocation.latitude),\(toLocation.longitude)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyDZV57Xyc4z_Nh6hi9uS52GrTQvmPFq2HU"
        
        Alamofire.request(url).responseJSON { response in
            do{
                let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String: Any]
                let routes = json["routes"] as! [[String: Any]]
                
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"] as! [String: Any]
                    let points = routeOverviewPolyline["points"] as! String
                    
                    DispatchQueue.main.async {
                        self.clear()
                        let path = GMSPath(fromEncodedPath: points)
                        let polyline = GMSPolyline(path: path)
                        polyline.strokeWidth = 3.0
                        polyline.strokeColor = AppTheme.colorSecondary
                        polyline.map = self
                        
                        let bounds = GMSCoordinateBounds(path: path!)
                        self.animate(with: GMSCameraUpdate.fit(bounds, withPadding: padding))
                        success()
                    }
                }
            } catch{}
        }
    }
}
