//
//  APIManager.swift
//  Hustle
//
//  Created by  Wasi Tariq on 01/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation
import Alamofire

class ServiceManager{
    
    static let shared = ServiceManager()
    private init(){}
    private let decoder = JSONDecoder()
    
    func request<T : Decodable> (service : Service , completion : @escaping(T?,Error?) -> Void)
    {
        print(service.parameters.dictionary)
        Alamofire.request(service.url!, method: service.method, parameters: service.parameters.dictionary, encoding : JSONEncoding.default, headers : service.headers as? HTTPHeaders).responseJSON { (response) in
            do{
                if response.response?.statusCode == 401{
                    // session has expired , user should be routed to login page now
                    (UIApplication.shared.delegate as! AppDelegate).viewModel.sessionExpired()
                }else{
                    print(response.result.value as Any)
                    let data = response.data!
                    let model = try self.decoder.decode(T.self, from: data)
                    completion(model,nil)
                }
            }
            catch {
                print("Error in decoding json \n error: \(error.localizedDescription)")
                completion(nil, error)
            }
        }
    }
    
    func getRequest<T: Decodable>(url: String, completion: @escaping(T?, Error?) -> Void){
        Alamofire.request(url).responseJSON { (response) in
            do{
                print(response.result.value as Any)
                let model = try self.decoder.decode(T.self, from: response.data!)
                completion(model, nil)
            }catch{
                print("Error in decoding json")
                completion(nil, error)
            }
        }
    }
}

class Service
{
    var url : String?
    var parameters = Parameters()
    var method : HTTPMethod = .post
    var headers = Dictionary<String, Any>()
    
    init(url : String) {
        self.url = url
    }
    
    convenience init (url : String , parameters : Parameters)
    {
        self.init(url: url)
        self.parameters = parameters
    }
    
    convenience init(url : String , method : HTTPMethod)
    {
        self.init(url: url)
        self.method = method
    }
    
    convenience init(url : String , parameters : Parameters , method : HTTPMethod) {
        self.init(url: url)
        self.parameters = parameters
        self.method = method
    }
    
    convenience init(url: String, parameters: Parameters, headers: Dictionary<String, Any>){
        self.init(url: url, parameters: parameters)
        self.headers = headers
    }
}


class Parameters
{
    var dictionary = Dictionary<String , Any>()
    
    init(){}
    
    init(keys : [String] , values : [Any]) {
        
        self.populateDictionary(keys: keys, values: values)
    }
    
    init(dictionary : Dictionary<String , Any>)
    {
        self.dictionary = dictionary
    }
    
    func populateDictionary(keys keyArray : [String] , values valueArray : [Any])
    {
        for i in 0..<min(keyArray.count, valueArray.count)
        {
            dictionary[keyArray[i]] = valueArray[i]
        }
    }
}
