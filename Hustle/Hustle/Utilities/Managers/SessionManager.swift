//
//  SessionManager.swift
//  Hustle
//
//  Created by  Wasi Tariq on 01/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

class SessionManager{
    
    private let sessionTokenKey = "SessionTokenKey"
    private let userDefaultKey = "SessionManagerKey"
    private let currentTripKey = "CurrentTripKey"
    private let countryCodeKey = "CountryCodeKey"
    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()
    private var userInfo: CustomerData?
    private var _isLoggedin = false
    private var sessionToken = ""
    private var fcmToken = ""
    private var currentTrip: String?
    private var currentCountryCode = CountryCodeType.arab
    
    static let shared = SessionManager()
    private init(){
        initializeWithUserDefaults()
    }
    
    private func initializeWithUserDefaults(){
        if let userData = UserDefaults.standard.value(forKey: userDefaultKey) as? Data{
            do{
                userInfo = try decoder.decode(CustomerData.self, from: userData)
                sessionToken = (UserDefaults.standard.value(forKey: sessionTokenKey) as? String) ?? ""
                currentTrip = (UserDefaults.standard.value(forKey: currentTripKey) as? String) ?? ""
                currentCountryCode = CountryCodeType(rawValue: (UserDefaults.standard.value(forKey: countryCodeKey) as? String ?? "")) ?? .arab
                self._isLoggedin = true
            }catch{
                print("Error in decoding data")
            }
        }
    }
    
    func loginWith(AccountInfo: CustomerData, token: String){
        do{
            let data = try encoder.encode(AccountInfo)
            UserDefaults.standard.set(data, forKey: userDefaultKey)
            UserDefaults.standard.set(token, forKey: sessionTokenKey)
            userInfo = AccountInfo
            self.sessionToken = token
            self._isLoggedin = true
        }catch{
            print("error in encoding json")
        }
    }
    
    func updateProfile(accountInfo: CustomerData){
        do{
            let data = try encoder.encode(accountInfo)
            UserDefaults.standard.set(data, forKey: userDefaultKey)
            userInfo = accountInfo
        }catch{
            print("error in encoding json")
        }
    }
    
    func registerFCMToken(_ token: String){
        self.fcmToken = token
    }
    
    func logOut(){
        UserDefaults.standard.set(nil, forKey: userDefaultKey)
        self._isLoggedin = false
        self.userInfo = nil
        self.sessionToken = ""
    }
    
    func getCurrentTripId() -> String{
        return self.currentTrip ?? ""
    }
    
    func setCurrentTrip(id: String){
        UserDefaults.standard.set(id, forKey: currentTripKey)
        self.currentTrip = id
    }
    
    func removeCurrentTrip(){
        UserDefaults.standard.removeObject(forKey: currentTripKey)
    }
    
    func isCurrentTripInProgress() -> Bool{
        let currentTrip = UserDefaults.standard.value(forKey: currentTripKey) as? String
        if let trip = currentTrip , !trip.isEmpty{
            return true
        }
        return false
    }
    
    func setCurrentCountryCode(_ code: CountryCodeType){
        UserDefaults.standard.set(code.rawValue(), forKey: countryCodeKey)
        self.currentCountryCode = code
    }
    
    func getCurrentCountryCode() -> CountryCodeType{
        return self.currentCountryCode
    }
    
    func isLoggedin() -> Bool{
        return self._isLoggedin
    }
    
    func getID() -> String{
        return (userInfo?.Id) ?? ""
    }
    
    func getEmail() -> String{
        if let user = userInfo{
            return user.EmailAddress ?? ""
        }
        return ""
    }
    
    func getFullName() -> String{
        if let user = userInfo{
            return (user.FirstName ?? "") + " " + (user.LastName ?? "")
        }
        return ""
    }
    
    func getFirstName() -> String{
        if let user = userInfo{
            return user.FirstName ?? ""
        }
        return ""
    }
    
    func getLastName() -> String{
        if let user = userInfo{
            return user.LastName ?? ""
        }
        return ""
    }
    
    func getPhoneNumber() -> String{
        if let user = userInfo{
            return user.PhoneNumber ?? ""
        }
        return ""
    }
    
    func getToken() -> String{
        return self.sessionToken
    }
    
    func getFCMToken() -> String{
        return self.fcmToken
    }
}
