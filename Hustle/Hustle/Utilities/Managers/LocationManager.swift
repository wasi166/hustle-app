//
//  LocationManager.swift
//  Hustle
//
//  Created by  Wasi Tariq on 01/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate{
    
    static let shared = LocationManager()
   
    private override init(){}
    private var lastKnownLocation: CLLocationCoordinate2D? // keeps the last received location of user, and will be updated through out the app cycle.
    private var locationManger: CLLocationManager?
    
    // start updating the location of the user and saves it in lastKnownLocation variable.
    func startUpdatingLocation(){
        locationManger = CLLocationManager()
        locationManger!.delegate = self
        locationManger!.desiredAccuracy = kCLLocationAccuracyBest
        locationManger!.startUpdatingLocation()
    }
    
    // stops updating the location of user, don't know when to call it, but implementing for future 🧐
    func stopUpdatingLocation(){
        locationManger!.delegate = nil
        locationManger!.stopUpdatingLocation()
        locationManger = nil
    }
    
    // return the last received location of the user.
    func lastReceivedLocation() -> CLLocationCoordinate2D?{
        return self.lastKnownLocation
    }
    
    // requests the location permission from the user
    func requestForLocationPermission(){
        locationManger?.requestAlwaysAuthorization()
        UserDefaults.standard.set(true, forKey: UserDefaultConstants.LocationPermissionAsked)
    }
    
    // returns true if user has seen the location permission dialog atleast once.
    func hasAskedForLocationPermission() -> Bool{
        if let permissionStatus = UserDefaults.standard.value(forKey: UserDefaultConstants.LocationPermissionAsked) as? Bool, permissionStatus{
            return true
        }
        return false
    }
    
    
    //MARK:- CLLocationDelegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if lastKnownLocation == nil && locations.first != nil{
            lastKnownLocation = locations.first!.coordinate
            NotificationCenter.default.post(name: .receivedLocationFirstTime, object: nil)
        }
        lastKnownLocation = locations.first?.coordinate
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error in getting location of user")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(status)
    }
}
