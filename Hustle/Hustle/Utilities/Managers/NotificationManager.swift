//
//  NotificationManager.swift
//  Hustle
//
//  Created by  Wasi Tariq on 18/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

enum RideStatusTypes: String{
    case busy = "Busy"
    case accepted = "Accepted"
    case cancelled = "Cancelled by driver"
    case requested = "Requested"
    case arrived = "Arrived"
    case inRide = "Started"
    case finished = "Finished"
    case paid = "Paid"
}

class NotificationManager{
    static let shared = NotificationManager()
    
    private init(){}
    
    func didRecieveNotification(data: Dictionary<String, Any>){
        if let notification = readAndParseNotificationData(dict: data),
            let status = notification.ride?.rideStatus?.Title{
            postInternalNotification(rideStatus: status, object: notification)
        }
    }
    
    private func readAndParseNotificationData(dict: Dictionary<String, Any>) -> NotificationDataModel?{
        do{
            if let dataString = dict["data"] as? String{
                let data = dataString.data(using: .utf8)
                let notificationModel = try JSONDecoder().decode(NotificationDataModel.self, from: data!)
                return notificationModel
            }
        }catch{
            debugPrint("error in decoding notification data")
        }
        return nil
    }
    
    private func postInternalNotification(rideStatus: String, object: NotificationDataModel){
        if let ridestatusType = RideStatusTypes(rawValue: rideStatus){
            switch ridestatusType{
            case .accepted:
                NotificationCenter.default.post(Notification(name: .driverAcceptedRide, object: object, userInfo: nil))
                
            case .arrived:
                NotificationCenter.default.post(Notification(name: .driverArrived, object: object, userInfo: nil))
                
            case .inRide:
                NotificationCenter.default.post(Notification(name: .rideStarted, object: object, userInfo: nil))
                
            case .finished:
                NotificationCenter.default.post(Notification(name: .rideEnded, object: object, userInfo: nil))
                
            case .cancelled:
                NotificationCenter.default.post(Notification(name: .driverCancelledRide, object: object, userInfo: nil))
                
            case .paid:
                break
                
            default:
                break
            }
        }
    }
    
    
}
