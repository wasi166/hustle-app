//
//  FileRead.swift
//  ProtectionPro
//
//  Created by Wasi Tariq on 4/8/19.
//  Copyright © 2019 Hustle. All rights reserved.
//

import Foundation

class FileRead {
    
    /**
        Reads the data in the file.
     
     - Parameter name: name of the file which should be declared on FileType enum.
     - Parameter ofType: type of the file to read e.g. plist,txt etc

     - Returns: Returns the data of the file as Any which can be of type Dictionary or Array or nil if data is invalid or file not found.
     **/
    static func getContentsOfFile(with name: FileType, ofType: String) -> Any? {
        
        if let path = Bundle.main.path(forResource: name.rawValue, ofType: ofType) {
            if let dictionary = NSDictionary(contentsOfFile: path) {
                return dictionary as? [String: Any]
            }
            if let array = NSArray(contentsOfFile: path) {
                return array
            }
        }
        return nil
    }
    
    /**
     get Serialized Model from the content of the file.

     - Parameter T: data model of the file and should conform to Decodable protocol.
     - Parameter fileName: name of the file which should be declared in FileTpe enum.
     - Parameter ofType: type of the file to read e.g. plist,txt etc

     - Returns: Serialized models for the file of type T and nil if file not found or data is in invalid format.
     **/
    
    static func getDataModel <T:Decodable> (for fileName: FileType, ofType: String) -> T? {
        if let fileContent = getContentsOfFile(with: fileName, ofType: ofType) {
            do {
                let data = try JSONSerialization.data(withJSONObject: fileContent, options: .prettyPrinted)
                return try JSONDecoder().decode(T.self, from: data)
            } catch {assertionFailure("Unable to parse Configuration file")}
        }
        return nil
    }
}



