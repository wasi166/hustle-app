//
//  Scheduler.swift
//  Hustle
//
//  Created by  Wasi Tariq on 22/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation
import UIKit //FIXME: should not import ui kit in this class

class Scheduler{
    var timeInterval = 0.0
    var url: String
    var params: Dictionary<String, Any>
    
    private var isAPICallInProgress = false
    private var timer: Timer!
    
    init() {
        self.url = ""
        self.timeInterval = 0.0
        self.params = [:]
    }
    
    init(url: String, timeInterval: Double = 0.0, params: Dictionary<String, Any> = [:]){
        self.timeInterval = timeInterval
        self.url = url
        self.params = params
        
    }
    
    func setValues(url: String, timeInterval: Double = 0.0, params: Dictionary<String, Any> = [:]){
        self.url = url
        self.timeInterval = timeInterval
        self.params = params
    }
    
    func startScheduler<T: Decodable>(response: @escaping(T?) -> Void){
        if url.isEmpty{
            return
        }
        
        timer = Timer.scheduledTimer(withTimeInterval: self.timeInterval, repeats: true, block: { (_) in
            if !self.isAPICallInProgress{
                self.isAPICallInProgress = true
                ServiceManager.shared.request(service:
                    Service(url: self.url,parameters: Parameters(dictionary: self.params), headers: (UIApplication.shared.delegate as! AppDelegate).viewModel.getAPIHeaders()),completion: { (model: T?, _) in
                        self.isAPICallInProgress = false
                        if let model = model{
                            response(model)
                        }
                })
            }
        })
    }
    
    func stopScheduler(){
        if self.timer != nil{
            self.timer.invalidate()
        }
    }
}
